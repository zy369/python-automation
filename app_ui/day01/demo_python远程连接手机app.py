# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-07-02 13:51
IDE: PyCharm
Introduction:
"""

from appium.webdriver import Remote
caps = {
  "platformName": "Android",
  "platformVersion": "7.1.2",
  "deviceName": "127.0.0.1:62001",
  "appPackage": "com.tencent.mobileqq",
  "appActivity": "com.tencent.mobileqq.activity.LoginActivity"
}
driver = Remote(desired_capabilities=caps, command_executor='http://127.0.0.1:4723/wd/hub')
driver.quit()