# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 14:28
IDE: PyCharm
Introduction:
"""
import time

from selenium import webdriver
driver=webdriver.Chrome('chromedriver.exe')
driver.implicitly_wait(10)
driver.get('https://www.12306.cn/index/')

driver.execute_script('arguments[0].readonly=false',driver.find_element_by_id('train_date'))
time.sleep(0.5)
driver.execute_script('arguments[0].value="2021-07-21"',driver.find_element_by_id('train_date'))
