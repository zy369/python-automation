# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 11:19
IDE: PyCharm
Introduction:
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver=webdriver.Chrome('chromedriver.exe')
driver.get('https://ke.qq.com/')
driver.find_element_by_id('js_login').click()
WebDriverWait(driver,5).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH,'//div[@class="login-qq-iframe-wrap"]/iframe')))
driver.find_element_by_id('switcher_plogin').click()