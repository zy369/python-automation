# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 11:32
IDE: PyCharm
Introduction:
"""
from selenium import webdriver

driver=webdriver.Chrome('chromedriver.exe')
driver.implicitly_wait(10)
driver.get('https://ke.qq.com/')
driver.find_element_by_id('js_login').click()
driver.switch_to.frame(driver.find_element_by_xpath('//div[@class="login-qq-iframe-wrap"]/iframe'))
driver.find_element_by_id('switcher_plogin').click()