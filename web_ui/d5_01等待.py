# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-29 13:57
IDE: PyCharm
Introduction:
"""


'''
selenium 的等待有3种： 强制等待、隐性等待、显性等待
WebDriverWait是个类，有until和until not 方法
expected_conditions 是selenium 的模块
'''
import time

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
driver=webdriver.Chrome('chromedriver.exe')
driver.get("https://wwww.baidu.com")
# 强制等待sleep
# time.sleep(2)
# 隐性等待 implicitly_wait
# driver.implicitly_wait(30 )
# 显性等待
WebDriverWait(driver, timeout=30).until(EC.visibility_of_element_located((By.ID,'kw')))
print(driver.current_url)
driver.quit()