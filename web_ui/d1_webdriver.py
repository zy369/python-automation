# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-28 16:59
IDE: PyCharm
Introduction:
"""
import time

from selenium import webdriver
driver=webdriver.Chrome('chromedriver.exe')
# driver.get("http://www.baidu.com")
# # 放大窗口
# driver.maximize_window()
# time.sleep(2)
# # 缩写窗口
# driver.minimize_window()
# # 设置窗口的大小
# driver.set_window_size( width=800, height=500, windowHandle='current')
driver.get('http://www.douban.com')
# 后退
driver.back()
time.sleep(2)
# 前进
driver.forward()
driver.refresh()
driver.close()
