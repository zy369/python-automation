# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-29 11:04
IDE: PyCharm
Introduction:
"""
'''
八种基本的元素定位方法： id,name,class,tag,link,partial_link,xpath,css
'''
from selenium import webdriver

driver=webdriver.Chrome('chromedriver.exe')
driver.get("https://www.baidu.com")
driver.maximize_window()
driver.implicitly_wait(30)
# 元素定位
## 1、id定位
# driver.find_element_by_id("kw").send_keys('sunny')
## 2、name定位
# driver.find_element_by_name('wd').send_keys('sunny')
## 3、class定位
# driver.find_element_by_class_name('s_ipt').send_keys('sunny')
# 不会用标签太容易重复了
## 4、tag定位
#driver.find_element_by_tag_name('input')
# 5、link定位
# driver.find_element_by_link_text("新闻").click()
## 6、partial_link定位
# driver.find_element_by_partial_link_text('闻').click()
## 7、xpath定位
# driver.find_element_by_xpath('//*[@id="kw"]').send_keys('sunny')
## 8、css定位
driver.find_element_by_css_selector('#kw').send_keys('sunny')