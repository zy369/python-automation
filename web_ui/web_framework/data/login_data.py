# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 17:57
IDE: PyCharm
Introduction:
"""
login_data_error = [
    {"mobile":"", "pwd": "", "expected": "请输入手机号"},
    {"mobile":"123", "pwd": "", "expected": "请输入正确的手机号"},
]


login_data_invalid = [
    {"mobile":"15746732896", "pwd": "123", "expected": "此账号没有经过授权，请联系管理员!"},
]


login_data_success = [
    {"mobile":"18684720553", "pwd": "python", "expected": "我的帐户[python]"},
]