# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 17:19
IDE: PyCharm
Introduction:
"""
import unittest

import unittestreport

suite=unittest.defaultTestLoader.discover(r'E:\pythonProject\python-automation\web_ui\web_framework\test_cases')
unittestreport.TestRunner(suite).run()