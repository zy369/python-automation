# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 15:46
IDE: PyCharm
Introduction:
"""
import time
import unittest
from selenium import webdriver
from web_ui.web_framework.pages.index_page import IndexPage
from web_ui.web_framework.pages.login_page import LoginPage
from unittestreport import ddt,list_data
from web_ui.web_framework.data.login_data import (login_data_error,login_data_invalid,login_data_success)

@ddt
class TestLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = webdriver.Chrome(
            r'E:\pythonProject\python-automation\web_ui\web_framework\library\chromedriver.exe')
        cls.driver.implicitly_wait(30)
        cls.index_page = IndexPage(cls.driver)
        cls.login_page = LoginPage(cls.driver)
        cls.driver.get('http://8.129.91.152:8765/login')

    def setUp(self) -> None:
        self.login_page.mobile_clear()
        self.login_page.pwd_clear()
    def tearDown(self) -> None:
        pass
    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.quit()

    @list_data(login_data_error)
    def test_login_error(self,test_info):
        self.login_page.login(test_info['mobile'], test_info['pwd'])
        time.sleep(1)
        actual=self.login_page.get_error_msg()
        expected=test_info['expected']
        self.assertIn(expected,actual.text)

    @list_data(login_data_success)
    def test_login_success(self,test_info):
        self.login_page.login(test_info['mobile'], test_info['pwd'])
        time.sleep(1)
        self.index_page.get()
        actual=self.index_page.get_element_user()
        expected=test_info['expected']
        self.assertIn(expected,actual.text)


if __name__ == '__main__':
    unittest.main()

