# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 16:58
IDE: PyCharm
Introduction:
"""
from selenium.webdriver.common.by import By

from web_ui.web_framework.common.base_page import BasePage


class LoginPage(BasePage):

    mobile_element_locator=(By.XPATH,'//input[@name="phone"]')
    pwd_element_locator=(By.XPATH,'//input[@name="password"]')
    button_element_locator=(By.XPATH,'//button[@class="btn btn-special"]')
    error_msg_locator=(By.XPATH, "//div[@class='form-error-info']")

    def login(self,mobile,pwd):

        url='http://8.129.91.152:8765/login'
        self.driver.get(url)
        self.get_mobile_element().send_keys(mobile)
        self.get_pwd_element().send_keys(pwd)
        self.driver.find_element(*self.button_element_locator).click()

    def get_mobile_element(self):
        return self.driver.find_element(*self.mobile_element_locator)

    def get_pwd_element(self):
        return self.driver.find_element(*self.pwd_element_locator)
    def get_error_msg(self):
        return self.driver.find_element(*self.error_msg_locator)
    def mobile_clear(self):
        self.get_mobile_element().clear()

    def pwd_clear(self):
        self.get_pwd_element().clear()
