# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 16:20
IDE: PyCharm
Introduction:
"""
class IndexPage:

    url='http://8.129.91.152:8765'

    def __init__(self, driver):
        self.driver = driver

    def get_element_user(self):
        """获取用户"""
        return self.driver.find_element_by_xpath("//a[@href='/Member/index.html']")

    def get(self):
        """打开当前页面"""
        self.driver.get(self.url)