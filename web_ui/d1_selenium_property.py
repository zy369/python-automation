# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-28 16:52
IDE: PyCharm
Introduction:
"""
from selenium import webdriver

driver=webdriver.Chrome('chromedriver.exe')
# 加载页面
driver.get('https://www.baidu.com')
# 获取当前页面url
print(driver.current_url)
# 获取当前html页面title标签的文本信息
print(driver.title)
# 获取当前html源码
print(driver.page_source)
# 获取所有页面的句柄
print(driver.window_handles)
# 获取当前页面的句柄
print(driver.current_window_handle)
