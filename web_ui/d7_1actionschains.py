# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-30 13:16
IDE: PyCharm
Introduction:
"""
from selenium import webdriver
from selenium.webdriver import ActionChains

driver=webdriver.Chrome('chromedriver.exe')
driver.maximize_window()
driver.implicitly_wait(5)
driver.get('http://www.baidu.com')
elem=driver.find_element_by_css_selector('.s_ipt')
c=driver.find_element_by_css_selector('#su')
ActionChains(driver).move_to_element(elem).send_keys('腾讯课堂').click(c).perform()
