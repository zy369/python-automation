"""
============================
Author:dao dao
Time:2021/6/19
E-mail:402583874@qq.com
============================
"""


import requests
from lxml import etree

headers = {
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36 QBCore/4.0.1326.400 QQBrowser/9.0.2524.400 Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2875.116 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) WindowsWechat(0x63010200)",

    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.5;q=0.4",
    "Cookie": "appmsg_token=1118_9ahVtRPJtgc2foZCj5zGbtfGUEBexHvH1T_XeMTwUrI8HkGd_RGGQXkiZQGHu8U61l9hD7Jtd83vnq0U; rewardsn=; wxuin=1806022388; devicetype=Windows7x64; version=6302019a; lang=zh_CN; pass_ticket=7y/Wla1+Z0PYbtRx8WORU5iMDg3eiR0Hp0zpee/k1DnDx4FJCNa9W46qBEKfvJRZ; wap_sid2=CPTtlt0GEooBeV9IQ2pFZXVkam83eEU2Yjd3azZkaDdZMnZULTZiTVZwZDlMM0QzWnVDWXZXVlRyc2tJSDNkREJMM1VLMlFEem9DV2wwX3plaV9rOG9wUTgzTUtPanRfTVlBN0tWdWdCcnZLZ2xSV2tuT0lTUmlTTDFzVkVXYVl6SGkxZ19JbHZFdmJqd1NBQUF+MOOttoYGOA1AAQ==; wxtokenkey=777"
}
api="https://mp.weixin.qq.com/s?__biz=MzAwNzIyNzIzMg==&mid=2651935862&idx=1&sn=2c7136c3f930375f3b69782d8cd0c8a3&chksm=80e4a679b7932f6f5e5c9f0ad51741cb71ce923343e9bfa086d3b253b78e271acc400be4d63e&scene=126&sessionid=1624066344&key=cf03c248aa559daef1f24acd85720e417040983a5eaaa5dffe8d4ebb38af440a1e55a9edad862a9e5edb42da6ca5b97445c60684d6bf81e4c2bd54c49b2ad9c8a037f1d85e300bc5e6955b80de8cb5289885a2403a9082fdeb31cdc0da31e2f51dbef8465c433daa200d2d009f64a3548b066c2d200d08f5bb81ec39a8546fdd&ascene=1&uin=MTgwNjAyMjM4OA%3D%3D&devicetype=Windows+7+x64&version=6302019a&lang=zh_CN&exportkey=A1FUA%2Fhifz2iIGVfv%2F8uokg%3D&pass_ticket=7y%2FWla1%2BZ0PYbtRx8WORU5iMDg3eiR0Hp0zpee%2Fk1DnDx4FJCNa9W46qBEKfvJRZ&wx_header=0&fontgear=2"
response=requests.get(url=api,headers=headers)
html=response.text
x=etree.HTML(html)
items=x.xpath('//*[@id="js_content"]/div/p/span/text()')
print(items)


