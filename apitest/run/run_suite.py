import unittest
import unittestreport
from apitest.common.contents import TESTCASES_DIR, REPORTS_DIR

suite = unittest.defaultTestLoader.discover(TESTCASES_DIR)
unittestreport.TestRunner(
    suite,
    filename="report.html",
    report_dir=REPORTS_DIR,
    title='测试报告',
    tester='测试员',
    desc="XX项目测试生成的报告",
    templates=1
).run(thread_count=5, count=0, interval=2)
# TODO: perfect
