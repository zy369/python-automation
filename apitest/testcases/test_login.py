"""
============================
Author:dao dao
Time:2021/6/15
E-mail:402583874@qq.com
=======
"""
import os
import unittest
from unittestreport import ddt,list_data
from apitest.common.contents import DATA_DIR
from apitest.common.handle_requests import HandleRequests
from apitest.common.myconfig import conf
from apitest.common.mylogger import my_log
from apitest.common.readexcel import ReadExcel

data_file_path=os.path.join(DATA_DIR,'apicases.xlsx')

@ddt
class TestLogin(unittest.TestCase):

    excel=ReadExcel(data_file_path,'login')
    cases=excel.read_data()
    http=HandleRequests()

    @list_data(cases)
    def test_login(self,case):

        url=conf.get_str('env','url')+case['url']
        method=case['method']
        data=eval(case['data'])
        headers=eval(conf.get_str('env','headers'))
        expected=eval(case['expected'])
        row=case['case_id']+1
        result=self.http.send(url=url,method=method,json=data,headers=headers).json()

        try:
            self.assertEqual(expected['code'],result['code'])
            self.assertEqual(expected['msg'],result['msg'])
        except AssertionError as e:
            self.excel.write_data(row=row,column=8,value='Failure')
            my_log.info(f'用例{case["title"]}-------->执行未通过')
            print(f'预期结束：{expected}')
            print(f'实际结果: {result}')
            raise e
        else:
            self.excel.write_data(row=row, column=8, value='Success')
            my_log.info(f'用例{case["title"]}-------->执行通过')

