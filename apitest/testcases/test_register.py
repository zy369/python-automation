import os
import random
import unittest

from unittestreport import ddt, list_data

from apitest.common.contents import DATA_DIR
from apitest.common.handle_requests import HandleRequests
from apitest.common.handledb import HandleDB
from apitest.common.myconfig import conf
from apitest.common.mylogger import my_log
from apitest.common.readexcel import ReadExcel

filename = os.path.join(DATA_DIR, 'apicases.xlsx')


@ddt
class TestRegister(unittest.TestCase):
    excel = ReadExcel(filename=filename, sheet_name='register')
    cases = excel.read_data()
    http = HandleRequests()
    db = HandleDB()

    @list_data(cases)
    def test_register(self, case):
        url = conf.get_str('env', 'url') + case['url']
        method = case['method']
        headers = eval(conf.get_str('env', 'headers'))
        row = case['case_id'] + 1
        if '#phone#' in case['data']:
            self.phone = self.random_phone()
            case['data'] = case['data'].replace('#phone#', self.phone)
        data = eval(case['data'])
        expected = eval(case['expected'])
        resulte = self.http.send(url=url, method=method, json=data, headers=headers).json()
        try:
            self.assertEqual(expected['code'], resulte['code'])
            self.assertEqual(expected['msg'], resulte['msg'])
            if resulte['msg'] == 'OK':
                sql = f"SELECT * FROM futureloan.member WHERE mobile_phone={self.phone}"
                count = self.db.count(sql)
                self.assertEqual(1, count)

        except AssertionError as e:
            self.excel.write_data(row=row, column=8, value='Failure')
            my_log.info(f'testcase{case["title"]}---->Failure')
            print(expected)
            print(resulte)
            raise e
        else:
            self.excel.write_data(row=row, column=8, value='Success')
            my_log.info(f'testcase{case["title"]}---->Success')

    @classmethod
    def random_phone(cls):
        phone = '188'
        for i in range(8):
            phone += str(random.randint(0, 9))
        return phone

    @classmethod
    def tearDownClass(cls) -> None:
        cls.db.close()
