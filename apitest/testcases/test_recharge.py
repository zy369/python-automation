"""
============================
Author:dao dao
Time:2021/6/17
E-mail:402583874@qq.com
=======
"""
import os.path
import unittest
from jsonpath import jsonpath
from unittestreport import ddt, list_data
from apitest.common.contents import DATA_DIR
from apitest.common.handle_requests import HandleRequests
from apitest.common.myconfig import conf
from apitest.common.mylogger import my_log
from apitest.common.readexcel import ReadExcel

filename = os.path.join(DATA_DIR, 'apicases.xlsx')


@ddt
class TestRecharge(unittest.TestCase):
    excel = ReadExcel(filename=filename, sheet_name='recharge')
    cases = excel.read_data()
    http = HandleRequests()

    @classmethod
    def setUpClass(cls):
        url = conf.get_str('env', 'url') + '/member/login'
        headers = eval(conf.get_str('env', 'headers'))
        data = {"mobile_phone": conf.get_str('login_data', 'mobile_phone'), "pwd": conf.get_str('login_data', 'pwd')}
        result = cls.http.send(url=url, method='post', json=data, headers=headers).json()
        cls.member_id = jsonpath(result, "$..id")[0]
        token_type = jsonpath(result, "$..token_type")[0]
        token = jsonpath(result, "$..token")[0]
        cls.Bearer_token = token_type + ' ' + token

    @list_data(cases)
    def test_recharge(self, case):
        url = conf.get_str('env', 'url') + case['url']
        method = case['method']
        headers = eval(conf.get_str('env', 'headers'))
        headers['Authorization'] = self.Bearer_token
        if '#member_id#' in case['data']:
            case['data'] = case['data'].replace('#member_id#', str(self.member_id))
        data = eval(case['data'])
        row = case['case_id'] + 1
        result = self.http.send(url=url, method=method, json=data, headers=headers).json()
        expected = eval(case['expected'])
        try:
            self.assertEqual(expected['code'], result['code'])
            self.assertEqual(expected['msg'], result['msg'])
        except AssertionError as e:
            self.excel.write_data(row=row, column=8, value='Failure')
            my_log.info(f'用例{case["title"]}----------->Failure')
            print(expected)
            print(result)
            raise e
        else:
            self.excel.write_data(row=row, column=8, value='Success')
            my_log.info(f'用例{case["title"]}----------->Success')
