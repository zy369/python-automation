# -*- coding: utf-8 -*- 
"""
Project: python-automation
Creator: sunny
Create time: 2021-06-21 10:51
IDE: PyCharm
Introduction:
"""

"""
官方网站 https://docs.python.org/zh-cn/3/library/re.html
特殊字符：
     点  []  \d  \s   \w    | + ? *  {m} {m,n}  {m,n}?  ()
     
"""
import re

# 一、单字符的表示规则
# (点) 在默认模式，匹配除了换行的任意字符
# re1=r'.'
# res=re.findall(re1,'a\nb\rc')
# print(res)

# []用于表示一个字符集合
# re2=r'[abc]'
# res=re.findall(re2,'abcd')
# print(res)

# \d 表示一个数字
# re3='\d'
# res=re.findall(re3,'1a')
# print(res)

#  \D 匹配任何非十进制数字的字符，就是\d取非
# res4='\D'
# res=re.findall(res4,'1a')
# print(res)

# \s 匹配任何Unicode空白字符（包括 [ \t\n\r\f\v]),匹配ASCII中的空白字符，就是 [ \t\n\r\f\v]
# res5='\s'
# res=re.findall(res5,'a b    c')
# print(res)

# \S 就是\s取非
# res5='\S'
# res=re.findall(res5,'a b    c')
# print(res)

# \w  对于 Unicode (str) 样式：
# 匹配Unicode词语的字符，包含了可以构成词语的绝大部分字符，也包括数字和下划线。如果设置了 ASCII 标志，就只匹配 [a-zA-Z0-9_] 。
#
# 对于8位(bytes)样式：
# 匹配ASCII字符中的数字和字母和下划线，就是 [a-zA-Z0-9_] 。如果设置了 LOCALE 标记，就匹配当前语言区域的数字和字母和下划线
# re6='\w'
# res=re.findall(re6,'1a_@')
# print(res)
# \W 就是\w 取反
# re6='\W'
# res=re.findall(re6,'1a_@')
# print(res)

# 多个

# |  A|B， A 和 B 可以是任意正则表达式，创建一个正则表达式，匹配 A 或者 B. 任意个正则表达式可以用 '|' 连接
# re2='abc|123'
# res=re.findall(re2,'abceflk123l')
# print(res)

# + 对它前面的正则式匹配1到任意次重复。 ab+ 会匹配 'a' 后面跟随1个以上到任意个 'b'，它不会匹配 'a'。
# re7='ab+'
# res=re.findall(re7,'abaaaaa')
# print(res)

# ? 对它前面的正则式匹配0到1次重复。 ab? 会匹配 'a' 或者 'ab'。
# re7='ab?'
# res=re.findall(re7,'abaaaaa')
# print(res)

# re1='abc'
# res=re.findall(re1,'abcd')
# print(re1)

#* 对它前面的正则式匹配0到任意次重复， 尽量多的匹配字符串。 ab* 会匹配 'a'，'ab'，或者 'a' 后面跟随任意个 'b'。
# re6='ab*'
# res=re.findall(re6,'aabbbb')
# print(res)


# {m} 对其之前的正则式指定匹配 m 个重复；少于 m 的话就会导致匹配失败
# re3='a{3}'
# res=re.findall(re3,'aaaa')
# print(res)

#{m,n}对正则式进行 m 到 n 次匹配，在 m 和 n 之间取尽量多。 比如，a{3,5} 将匹配 3 到 5个 'a'。忽略 m 意为指定下界为0，忽略 n 指定上界为无限次。
# res4='a{3,5}'
# res=re.findall(res4,'aaaaaa')
# print(res)


#{m,n}?前一个修饰符的非贪婪模式，只匹配尽量少的字符次数。比如，对于 'aaaaaa'， a{3,5} 匹配 5个 'a' ，而 a{3,5}? 只匹配3个 'a'。
# 加？ 关闭贪婪模式，匹配最少
# res5='a{3,5}?'
# res=re.findall(res5,'aaaaaa')
# print(res)

# 边界
# (插入符号) 匹配字符串的开头
# reg='^a'
# res=re.findall(reg,'aba')
# print(res)

# $ 匹配字符串尾
# reg='foobar$'
# res=re.findall(reg,r'aa111foobar')
# print(res)

# \b 匹配空字符串，但只在单词开始或结尾的位置
# r=r'\bfoo\b'
# res=re.findall(r,r'a foo a')
# print(res)
#\B 匹配空字符串，但 不 能在词的开头或者结尾 就是\b取非

#  （组合），匹配括号内的任意正则表达式，并标识出组合的开始和结尾
# r=r'a(\d{3})a'
# res=re.findall(r,'a123ab123b')
# print(res)

#re.search(pattern, string, flags=0)扫描整个 字符串 找到匹配样式的第一个位置，并返回一个相应的 匹配对象。如果没有匹配，就返回一个 None ； 注意这和找到一个零长度匹配是不同的。
# pattern='a'
# s='abc'
# res=re.search(pattern=pattern,string=s)
# print(res)
# prog = re.compile(pattern)
# result = prog.match(s)
# print(result)
# result=re.match(pattern,s)
# print(result)

# def sub(pattern, repl, string, count=0, flags=0):
# pattern=r'def\s+([a-zA-Z_][a-zA-Z_0-9]*)\s*\(\s*\):'
# repl=r'static PyObject*\npy_\1(void)\n{'
# s='def myfunc():'
# res=re.sub(pattern=pattern,repl=repl,string=s )
# print(res)
#
# def dashrepl(matchobj):
#     if matchobj.group(0) == '-': return ' '
#     else: return '-'
#
# res=re.sub('-{1,2}', dashrepl, 'pro----gram-files')
# print(res)
# res=re.sub(r'\sAND\s', ' & ', 'Baked Beans And Spam',flags=re.IGNORECASE)
# print(res)

# pattern = re.compile("d")
# res=pattern.search("dog")
# print(res)
# res=pattern.match('dog')
# print(res)
#  匹配对象总是有一个布尔值 True。如果没有匹配的话 match() 和 search() 返回 None 所以你可以简单的用 if 语句来判断是否匹配
# pattern=r'a'
# s='ans'
# match = re.search(pattern=pattern, string=s)
# if match:
#     print(match)

#Match.group([group1, ...])
'''返回一个或者多个匹配的子组。如果只有一个参数，结果就是一个字符串，如果有多个参数，
结果就是一个元组（每个参数对应一个项），如果没有参数，组1默认到0（整个匹配都被返回）。 
如果一个组N 参数值为 0，相应的返回值就是整个匹配字符串；如果它是一个范围 [1..99]，
结果就是相应的括号组字符串。如果一个组号是负数，或者大于样式中定义的组数，一个 IndexError 索引错误就 raise。
如果一个组包含在样式的一部分，并被匹配多次，就返回最后一个匹配。'''

m = re.match(r"(\w+) (\w+)", "Isaac Newton, physicist")
print(m.group(0))       # The entire match
print(m.group(1))       # The first parenthesized subgroup.
print(m.group(2))       # The second parenthesized subgroup.
print(m.group(1, 2))    # Multiple arguments give us a tuple.
print(m.group())        # The entire match
