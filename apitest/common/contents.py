# -*- coding: utf-8 -*-
# @Time    : 2021/6/15 14:01
# @Author  : daodao
# @FileName: contents.py
# @Software: PyCharm
# @qq ：402583874
"""
此模块处理项目路径
"""
import os

# 项目路径
BASEDIR = os.path.dirname(os.path.dirname(__file__))
# 配置文件路径
CONF_DIR = os.path.join(BASEDIR, 'conf')
DATA_DIR = os.path.join(BASEDIR, 'data')
LIBRARY_DIR = os.path.join(BASEDIR, 'library')
REPORTS_DIR = os.path.join(BASEDIR, 'reports')
TESTCASES_DIR = os.path.join(BASEDIR, 'testcases')
LOG_DIR = os.path.join(BASEDIR, 'log')
