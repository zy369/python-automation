# -*- coding: utf-8 -*-
# @Time    : 2021/6/15 15:35
# @Author  : daodao
# @FileName: myconfig.py
# @Software: PyCharm
# @qq ：402583874
import os
from configparser import ConfigParser
from apitest.common.contents import CONF_DIR


class MyConf(object):

    def __init__(self, filename, encoding='utf-8'):
        """
        :param filename:
        :param encoding:
        """
        self.filename = filename
        self.encoding = encoding
        self.conf = ConfigParser()
        self.conf.read(filename, encoding)

    def get_str(self, section, option):
        return self.conf.get(section, option)

    def get_int(self, section, option):
        return self.conf.getint(section, option)

    def get_float(self, section, option):
        return self.conf.getfloat(section, option)

    def get_bool(self, section, option):
        return self.conf.getboolean(section, option)

    def write_data(self, section, option, value):
        self.conf.set(section, option, value)
        self.conf.write(open(self.filename, 'w', encoding=self.encoding))


conf_path = os.path.join(CONF_DIR, 'conf.ini')
conf = MyConf(conf_path)
if __name__ == '__main__':
    print(conf.get_str('env', 'url'))
