"""
============================
Author:dao dao
Time:2021/6/15
E-mail:402583874@qq.com
=======
"""
import openpyxl


class CaseData(object):
    pass


class ReadExcel(object):

    def __init__(self, filename, sheet_name):

        self.filename = filename
        self.sheet_name = sheet_name

    def open(self):

        self.workbook = openpyxl.load_workbook(self.filename)
        self.sheet = self.workbook[self.sheet_name]

    def close(self):
        self.workbook.close()

    def read_data(self):
        self.open()
        rows = list(self.sheet.rows)
        title = [r.value for r in rows[0]]
        cases = []
        for row in rows[1:]:
            data = []
            for i in row:
                data.append(i.value)
            case = dict(zip(title, data))
            cases.append(case)
        self.close()
        return cases

    def read_data_obj(self):
        self.open()
        rows = list(self.sheet.rows)
        title = [r.value for r in rows[0]]
        cases = []
        for row in rows[1:]:
            data = []
            for r in row:
                data.append(r.value)
            case = list(zip(title, data))
            case_obj = CaseData()
            for k, v in case:
                setattr(case_obj, k, v)
            cases.append(case_obj.__dict__)
        self.close()
        return cases

    def write_data(self, row, column, value):

        self.open()
        self.sheet.cell(row=row, column=column, value=value)
        self.workbook.save(self.filename)
        self.close()


if __name__ == '__main__':
    excel = ReadExcel(r'F:\python-automation\py24_api_test\library\apicases.xlsx', 'login')
    # data=excel.read_data()
    data = excel.read_data_obj()
    print(data)
    print(*data)
