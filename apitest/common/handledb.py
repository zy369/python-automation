import pymysql
from apitest.common.myconfig import conf


class HandleDB(object):

    def __init__(self):
        self.con = pymysql.connect(user=conf.get_str('mysql', 'user'),
                                   password=conf.get_str('mysql', 'password'),
                                   host=conf.get_str('mysql', 'host'),
                                   port=conf.get_int('mysql', 'port'),
                                   charset=conf.get_str('mysql', 'charset')
                                   )
        self.cur = self.con.cursor()

    def get_one(self, sql):
        self.con.commit()
        self.cur.execute(sql)
        return self.cur.fetchone()

    def get_all(self, sql):
        self.con.commit()
        self.cur.execute(sql)
        return self.cur.fetchall()

    def count(self, sql):
        self.con.commit()
        self.cur.execute(sql)

    def close(self):
        self.cur.close()
        self.con.close()
