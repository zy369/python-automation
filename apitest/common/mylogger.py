"""
============================
Author:dao dao
Time:2021/6/15
E-mail:402583874@qq.com
=======
"""
import os
import logging
from apitest.common.contents import LOG_DIR
from apitest.common.myconfig import conf

level = conf.get_str('logging', 'level')
f_level = conf.get_str('logging', 'f_level')
s_level = conf.get_str('logging', 's_level')
filename = conf.get_str('logging', 'filename')
file_path = os.path.join(LOG_DIR, filename)


class MyLogger(object):

    @classmethod
    def create_logger(cls):
        my_log01 = logging.getLogger('dado')
        my_log01.setLevel(level)
        sh = logging.StreamHandler()
        sh.setLevel(s_level)
        my_log01.addHandler(sh)
        fh = logging.FileHandler(filename=file_path, encoding='utf-8')
        fh.setLevel(f_level)
        my_log01.addHandler(fh)
        formatter = logging.Formatter('%(asctime)s - [%(filename)s-->line:%(lineno)d] - %(levelname)s: %(message)s')
        sh.setFormatter(formatter)
        fh.setFormatter(formatter)
        return my_log01


my_log = MyLogger.create_logger()
