# -*- coding: utf-8 -*-
# @Time    : 2021/6/15 14:10
# @Author  : daodao
# @FileName: handle_requests.py
# @Software: PyCharm
# @qq ：402583874
import requests


class HandleRequests(object):
    """根据不同请求方法发送HTTP请求"""

    def send(self, url, method, params=None, data=None, json=None, headers=None):
        method = method.upper()
        if method == 'GET':
            return requests.get(url=url, params=params)
        elif method == 'POST':
            return requests.post(url=url, json=json, data=data, headers=headers)
        elif method == 'PATCH':
            return requests.patch(url=url, json=json, data=data, headers=headers)


class HandleSessionRequests():
    """处理Session鉴权的接口"""

    def __init__(self):
        self.se = requests.session()

    def send(self, url, method, params=None, data=None, json=None, headers=None):
        if method == 'GET':
            return self.se.get(url=url, params=params)
        elif method == 'POST':
            return self.se.post(url=url, json=json, data=data, headers=headers)
        elif method == 'PATCH':
            return self.se.patch(url=url, json=json, data=data, headers=headers)


if __name__ == '__main__':
    login_url = "http://api.lemonban.com/futureloan/member/login"
    headers = {
        "X-Lemonban-Media-Type": "lemonban.v3",
        "Content-Type": "application/json"
    }

    login_data = {
        "mobile_phone": "18888888844",
        "pwd": "12345678"
    }

    # res=HandleRequests().send(url=login_url,method='POST',json=login_data,headers=headers)
    res = HandleSessionRequests().send(url=login_url, method='POST', json=login_data, headers=headers)
    print(res.json())
