# python基础
## 引号 print 
```
"""
多行注释
第一行注释内容
第二行注释内容
第三行注释内容
"""
'''
多行注释2
第一行注释内容
第二行注释内容
第三行注释内容
'''
#print
print('helloword')#print 打印的内容会自动换行
print('helloword')
print('helloword',end='')# 加上end='',print打印的内容就不会自动换行
print('helloword')

#字符串 用引号包起来的内容叫做字符串：单引号、双引号、三个单引号、三个双引号
print('123')
print("123")
print('成绩："90"')# 字符串中有双引号就用单引号包起来
print("成绩：'90'")# 字符串中有单引号就用双引号包起来
print("""
姓名：小明

年龄：18

性别：男

""")
print('''
姓名：小红
年龄：18
性别：男
姓名：小红
年龄：18
性别：男
姓名：小红
年龄：18
性别：男
姓名：小红
年龄：18
性别：男
姓名：小红
年龄：18
性别：男
姓名：小红
年龄：18
性别：男
''')

```
## 变量
```
'''
变量
变量的命名规范：只可以用字母、数字、下划线组成（不能用数字开头）
变量的命名要见名知意（尽量不要使用拼音）

常见的命名方法（风格）：
    1、纯小写字母，每个单词之间用下划线隔开
    max_num=999
    2、大驼峰命名法：每个单词的第一个字母大写
    Max_Num=999
    3、小驼峰命名法：第一个单词首字母小写，后面单词首字母大写
    max_Num=999
'''
name='小明'
age=18
gender='男'
print(name,age,gender)
#不要使用拼音命名
xiaoming='小明'
```
## 标识符
```
'''
标识符：只要自己起的名字都是标识符
    变量、函数名、类名、模块名（py文件)、包名
标识符的命名规范：只可以用字母、数字、下划线组成（不能用数字开头）
变量的命名要见名知意（尽量不要使用拼音）

标识符命名时不能有python关键字
python关键字：每个关键字都是有特殊作用的  36个关键字
['False', 'None', 'True', '__peg_parser__', 'and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue',
'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda',
'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']



'''
import keyword
print(keyword.kwlist)# 在控制台输出所有的python关键字
print(len(keyword.kwlist))
```
## print 和 input
```
'''
print:输出
input:输入
'''
name=input('请输入你的名字：')
age=input('请输入你的年龄：')
gender=input('请输入你的性别：')
print(name,age,gender)
```
## 数值类型的数据
```
'''              
数值类型的数据          
整数：int（整数型）      
小数：float(浮点型)    
布尔值：bool（一种特殊的数据类
复数 不学            
type: 内置函数，用来查看数据
'''              
# 整数             
a=20             
print(type(a))   
# 浮点数            
b=1.2            
print(type(b))   
# 布尔类型           
t=True           
print(t,type(t)) 
```
## python 中的运算符
```
## 算术运算符                        
"""                             
+ ：加                            
- ：减                            
* ：乘                            
/ ：除                            
** ：幂运算                         
% ：取余                           
// ：除法取整                        
"""                             
# a=20                          
# b=5                           
# res = a/b                     
# print(res)                    
# print(2**4)                   
# print(9//5)                   
                                
# 赋值运算符     =   +=  -=  *=  /=  
# a=22                          
# a+=1 # a=a+1                  
# print(a)                      
#                               
#比较运算符  比较条件的成立，返回的是True , 不成立返回
'''                             
==  表示等于                        
！=  表示不等于                       
'''                             
# a=10                          
# b=10                          
# print(a==b)                   
# print(a>b)                    
                                
# 逻辑运算符： 与 或 非                  
'''                             
and  真真为真（所有的条件都要成立，才会返回True,否则返
or : 一真为真，假假为假 （只要一个条件成立，就返回True
not: 取反                         
'''                             
print(10<8 and 9<10)            
print(10<8 or 9<10)             
print(not 10<7)                 
                                
# 运算符的优先级 ： 用括号解决               
print(10>8 and (11>9 or 11<19 ))
```
## 随机数模块的使用
```
#导入随机数模块
import random

# 指定范围生成一个随机整数（包含边界）
num= random.randint(1,3)
print(num)
# 随机生成一个0-1之间的小数p [左闭右开 边界值包含0不包含1)
num1=random.random()
print(num1)

# 如何生成10到20 之间的小数
num2=random.randint(10,20)
num3=random.random()
num4=num2+num3
print(num4)
# 源码查看方式： ctrl + 鼠标左键点击

# 解决浮点运算的精度问题
import  decimal
a=2.89
b=0.3
print(a-b)
aa=decimal.Decimal('2.89')
bb=decimal.Decimal('0.3')
print(aa-bb)
```
## 字符串类型的基本操作
```
'''
字符串的定义： 通过引号（单引号、双引号、三引号）
注意点：
    空字符：
    空白字符：

'''
# 空字符：字符中没有任何内容
s1=''
print(s1)
# 空白字符：字符中有内容,内容是空格键，tab键等
s2=' '
print(s2)
s3= '   '
print(s3)
# python中任何数据类型都可以转换成bool值
#数据中有内容，转换成bool值后为：True
#数据中没有内容，转换成bool值后为：False
print(bool(s1))
print(bool(s2))
print(bool(s3))
print(type(s1))
print(type(s2))
print(type(s3))

num=999
print(type(999))
# 数值类型转换成字符串
s4=str(num)
print(s4,type(s4))

#字符串转换成数值
s='555'
num=int(s)
print(num,type(num))

# 下标索引取值
# 字符串的下标，从0开始（从前往后），从-1开始（从后往前）
str1='python hello'
res=str1[-4]
print(res)

# 字符串的切片操作
'''
[起始位置：终止位置] ： 取头不取尾，从起始位置开始，到终止位置前一个
起始位置： 不写，默认从头开始
终止位置： 不写，默认到结束
[起始位置：终止位置：步长] ：步长为多少，就是多少个取一个
'''
res=str1[:7]
print(res)
str2='12345678'
res=str2[::2]
# 12 34 56 78 9
print(res)
res=str2[::3]
#123 456 789
print(res)
#字符串拼接
desc1='我的名字叫daodao'
desc2='今年年龄是18岁'
# 第一种 +
res=desc1+desc2
print(res)
'''
# 第二种：join 方法
#   1、字符串X.join((字符串1，字符串2，字符串3，。。。))
    2、字符串1 字符串X 字符串2 字符串X 字符串3 字符串X
    
'''
res2=','.join((desc1,desc2,'python666'))
print(res2)
```
## 字符串的相关方法
```
'''
字符串的方法：find  count  replace split upper lower
方法的调用：字符串.方法名（）

'''
# find
s1 = "abcd234abcabcd234abc"
# 查找子串在主串中的位置，从前往后找，找到匹配的就返回子串的位置（子串第一个字母在主串中的位置）
# find(a,b) 第一个参数是子串，第二个参数是查找的起点
res=s1.find('s',4)
print(res)

# count 计算子串在主串中出现的次数
res=s1.count('abc')
print(res)

# replace 替换指定的子串
# 参数1：新的子串
# 参数2：旧的子串
# 参数3：替换的此数 (默认是全部)
res=s1.replace('abc','ABC',1)
print(res)

# split 指定分割点对字符串进行fenge
#参数1：分割点
#参数2：分割次数（默认是全部）
res=s1.split('a',1)
print(res)

# upper 将小写字符转换成大写字母
s='abc123CDEF'
res=s.upper()
print(res)

#lower 将大写字符转换成小写
res=s.lower()
print(res)
```
## 字符串格式化输出
```
'''
字符串的格式化输出
1、传统的格式化方法：%
2、format格式化输出
3、F表达式

'''
# 有几个{}，format 就可以传入几个数据，按照位置一一对应填入
str1 = "今收到{}，交了学费{}元，开此收据为凭证"
print(str1.format('小明',500))
# 通过下标指定位置填入数据
str1 = "今收到{0}，交了学费{0}元，开此收据为凭证".format('小明',500)
print(str1)
# 保留小数点后面的位数
print('今天XX股票的价格是：{:.4f}'.format(7))
# 将小数按照百分比的形式显示
print('百分比为：{:.2%}'.format(0.12))

# 格式化字符串长度
print('{:<8}******'.format('abc'))#左对齐8个字符不够补空格
print('{:9<8}******'.format('abc'))
print('{:>8}******'.format('abc'))#右对齐8个字符不够补空格
print('{:9>8}******'.format('abc'))
print('{:^8}******'.format('abc'))#中间对齐8个字符不够补空格
print('{:y^8}******'.format('abc'))

# 传统的格式化输入法：%
# %s 为字符占位（任意类型都可以）
# %d 为数值类型占位
# %f  为浮点类型占位
s='abd123--%s--%s--%s'%('A','B','C')
print(s)
s='我的名字%s,体重%dkg,性别%s'%('小明',80,'男')
print(s)
# F表达式格式化输出
name='小明'
age=18
s=F'我的名字{name}，年龄{18}'
print(s)

```
## 列表的基本使用
```
'''
列表：是一种可变的数据类型
列表的定义[]
'''
#列表可以通过下标取值
li = [19, 1.3, "python"]
print(li[2])
# 列表的切片操作
li1 = [11, 22, 33, 44, 55]
print(li1[::2])

# 列表中的增删改查的方法
li2=[1,2,3]
## 增加元素的方法 append   insert   exend
# append 向列表尾处添加一个元素（追加）
li2.append('11')
print(li2)
# insert 通过下标指定位置，向列表中加入数据
li2.insert(1,999)
print(li2)
# exend 一次性加入多个元素（放在列表末尾）
li2.extend(['a','b','c'])
print(li2)

# 删除列表中元素的方法  remove   pop  clear
# remove  删除指定的元素
li3 = [1, 2, 3, 11, 22, 33, 1, 2, 3]
li3.remove(1)
print(li3)
#pop  指定下标位置删除元素（默认删除最后一个元素）
li3.pop(3)
print(li3)
# clear 清空列表
li3.clear()
print(li3)

# 查找的方法 index
# index: 查找指定元素的下标，找到后返回下标值，没找到报错
li4=[1, 2, 3, 11, 22, 33, 1, 2, 3]
res=li4.index(11)
print(res)
# count: 统计列表中某个元素出现的次数
res=li4.count(1)
print(res)
# 修改表中某个元素的值，通过下标去指定元素，进行重新赋值
li5=["aa", "bb", 11, 22, 33]
li5[1]=999
print(li5)

# 其他方法
# sort  排序
li=[111, 22, 31, 41, 5, 6, 888, 8, 34, 8, 12, 7, 33]
li.sort() # 对列表进行升序排序
print(li)
li.sort(reverse=True)# 通过参数reverse=True 可以从大到小，降序排列
print(li)
# reverse 方法 将列表头元素变成尾元素，列表尾元素变成头元素
li.reverse()# 将列表反转（头变成尾，尾变成头）
print(li)
# copy 方法 复制 （将原有的列表在内存复制一遍）
li6=li.copy()
li7=li.copy()
print(id(li))
print(id(li6))
print(id(li7))
```
## 身份运算符和成员运算符
```
'''
成员运算符 in  , not in
'''
li = ["aa", "bb", 11, 22, 33]
print('aa'in li )
print('aa' not in li)
'''
身份运算符 is ,  is not
id: 查看数据的内存地址
'''
a=1000 
b=1000
c=1000
print(a is b)
print(a is not b)
print(id(a))
print(id(b))
print(id(c))
li=[11,22,33]
li2=[11,22,33]
print(id(li))
print(id(li2))
```
## 元组的使用
```
'''
列表：[]
元组 ：
    定义通过（） 来定义元组
    元组的数据可以是任意的
    元组是不可变的数据类型
    元组定义后：不能修改元素
元组的方法：
    元组只有两个查找的方法  count 查找元素在元组中出现的次数  index 方法 查找元素在元组中的下标
注意点
空元组怎么定义
    tu=()
元组中只有一个元素怎么定义
    tu=(11,)
'''
# tu=(11,22,33)
# tu[0]=44  #TypeError: 'tuple' object does not support item assignment
# print(tu)
# tu2=(11,22,33)
# res=tu2.count(11)
# print(res)
# res=tu2.index(11)# 查找元素在元组中的下标
# print(res)
# res=tu2.index(333)#ValueError: tuple.index(x): x not in tuple
# print(res)
#空元组的定义
# tu=()
# print(tu,type(tu))
# # 只有一个元素的元组定义
# tu=(11,)
# print(tu,type(tu))
```
## 字典的使用
```
'''
list  []
tuple  ()
dict   {}
字典的定义：通过{} 来表示字典
字典中的每一条数据是由key:value 组成，每对键值用逗号隔开
字典中的key是唯一的
key 只能是不可变的数据类型
value 可以是任意数据类型
不可变数据类型：数值、字符串、tuple

字典是可变数据类型支持增删改查的操作
    增加和修改 通过键 update() 一次增加多个元素
    删除  pop() popitem()  clear() del 
    查询  通过键查询，get()  keys() values()  items() 
'''
#字典的定义
dict1={'name':'小明','age':18,'height':180.00}
print(dict1,type(dict1))
# 空字典的定义
dict2={}
print(dict2,type(dict2))

#字典的增加和修改方法
dic3 = {"aa": 11, "bb": "22", "cc": 22}
dic3['dd']=99# 通过键可以增加元素
print(dic3)
dic3['aa']=999
print(dic3)# 通过键可以修改元素
dic3.update({'a':1,'b':2})# update 一次增加多个元素
print(dic3)

# 字典的删除方法
dic3.pop('aa')# pop() 通过指定键删除元素
print(dic3)
dic3.popitem()# popitem 删除字典中最后一个元素
print(dic3)
dic3.clear()# clear() 清空字典
print(dic3)

# dic4 = {"aa": 11, "bb": "22", "cc": 22}
# del dic4['aa']  # del 删除数据的关键字 可以删除任何数据
# print(dic4)
# del dic4
# print(dic4)

# 字典的查询方法
dic = {"aa": 11, "bb": "22", "cc": 22}
print(dic['aa']) # 通过键查询值
print(dic.get('aa'))# get 方法，通过键查询对应的值
print(dic.keys(),type(dic.keys())) # keys() 查询字典中所有的键
print(dic.values(),type(dic.values())) # values() 查询字典中所有的值
print(dic.items(),type(dic.items())) # items() 查询字典中所有的键值对
print(dic) # print 打印输出字典

# 字典第一的方式扩展
## 第一种 {} 定义字典
dic5 = {'aa': 999, 'bb': '22', 'cc': 22, 'dd': 999, 'ff': 999, 'a': 1, 'b': 1, 'c': 3}
## 第二种 dict()
dict6=dict(name="小明", age=18, gender="男")
print(dict6)

## 第三中 dict
#[("aa", 11), ("bb", 22), ("cc", 33)] 列表嵌套元组
dict7=dict([("aa", 11), ("bb", 22), ("cc", 33)])
print(dict7)
```
## 集合的使用
```
'''
set 的定义：通过{} 来表示
空集合的定义
    s=set()
set 是可变的数据类型
特性：
    1、元素不可以重复
    2、存放的元素是不可变数据类型
使用场景
    1、对列表去重
    2、区分可变和不可变数据类型
不可变数据类型： 数值、字符、元组
可变数据类型：列表、字典、集合
按照数据结构分类：
1、数值类型 ：整数、浮点数、布尔值
2、序列类型：字符串、列表、元组（可以下标取值，支持切片）
3、散列类型： 字典 集合

使用频率
列表 > 字典 >元组

'''
set1={11,22,33}
print(set1,type(set1))
set2=set() # 定义空集合
print(set2,type(set2))

# 去重
set3 = {1, 1, 1, 1, 12, 2, 2, 2, 2, 2, 3, 3, 3}
print(set3)
# 列表快速去重
li = [11, 22, 11, 22, 33, 11, 22, 33, 1, 2, 3]
print(list(set(li)))
#区分可变不可变类型
# li=[1,2,3]
# set4={li}
# print(set4) #TypeError: unhashable type: 'list'
# dic={'a':1}
# set5={dic}
# print(set5)#TypeError: unhashable type: 'dict'
tu=(1,2,3)
set6={tu}
print(set6)
```
## 条件判断
```
'''
if 语法规则：
    if 条件：
        条件成立时会执行的代码
    else:
        条件不成立时会执行的代码
python 中是通过：（冒号）来区分代码块的

一个条件语句
    可以由单独的if来组成：（条件成立需要做：事情1，条件不成立不需要处理）
    也可以由if----else-----来组成：（条件成立需要做：事情1，不成立需要做事情2）
    也可以由if----elif :有多个条件，不同的条件需要做不同事情
    还可以由 if---elif---...else 来组成（有多个条件，不同的条件需要做不同的事情，所有的条件不成立也要做特殊处理）
'''
score=float(input('请输入成绩：'))
# if score >60:
#     print('考试及格')
# else:
#     print('考试不及格')
#
if score<60:
    print('考试不及格')
# 考试成绩等级区分
'''
40 > x            E
40<= x <60        D 
60<= x <75        C
75<= x <85        B
85<= x <=100      A
'''
if score <40:
    print('E')
elif 40<=score<60:
    print('D')
elif 60<=score<75:
    print('C')
elif 75<=score<85:
    print('B')
elif 85<=score<=100:
    print("A")
else:
    print('输入错误')
```
## 条件判断登陆案例
```
'''
登陆的小案例
关键字pass:没有任何语义，占个位置，表示通过的意思
'''
user = {"user": "python", "pwd": "123"}
username=input('请输入有户名：')
password=input('请输入密码：')
if username==user['user'] and password==user['pwd']:
    print('登陆成功')
else:
    print('用户名或密码错误')
```
## while 循环
```
'''
while 条件：
    循环体
'''
a=10
while a>1:
    print(f'循环体{a}')
    a-=1
# 需求：打印100遍hello python,
# 需求：第50遍打印两遍
n=100
while n>=1:
    print(f'第{n}hello python')
    if n==50:
        print(f'第{n}hello python')
    n-=1
```
## 循环break
```
'''
break 和 continue
break:终止循环跳出循环体
continue: 终止当前本轮循环，开启下轮循环

while 中的else
循环条件不成立，退出循环执行else中的代码
使用break 跳出循环不会执行else中的代码
'''
# 计算 1+2+3+....100
n=1
s=0
# while n<=10:
#     s+=n
#     print(f'n=={n},s=={s}')
#     if n==5:
#         break
#     n+=1
# print(f'n=={n},和等于{s}')
# while n<=10:
#     s+=n
#     print(f'n=={n},s=={s}')
#     n += 1
#     if n==5:
#         continue
#
# print(f'n=={n},和等于{s}')

# while n<10:
#     print(n)
#     n += 1
#     if n==5: #循环条件不成立，退出循环执行else中的代码
#         break
#     else:
#         print('打印while循环中的else')
while n<10:
    print(n)
    n += 1
    if n==5:
        break #使用break 跳出循环不会执行else中的代码
else:
    print('打印while循环中的else')
```
## for 循环
```
'''
for 循环 遍历循环
range(): 左闭右开
range(n)
range(n,m)
range(n,m,k)
'''
li = ["小明", "小张", "小李"]
for name in li :
    print(name)
li2=list(range(100))
print(li2)
# 遍历列表
for i in li2:
    print(f'第{i}遍hello pthon')
#遍历range
for i in range(100):
    print(f'第{i}遍hello pthon')
# 遍历字符串
s = "abchgd"
for i in s:
    print(i)
dic = {"aa": 11, "bb": 22, "cc": 33}
# 遍历字典
for d in dic:
    print(d)# 直接遍历得到的是键
for k in dic.keys():
    print(k)

for v in dic.values(): # 遍历字典值
    print(v)

for k,v in dic.items():# 遍历字典键值对
    print(k,v)

#元组拆包
tu=(11,22)
a,b=tu
print(a,b)
# 同时定义两个变量
a,b=888,999
print(a,b)
```
## for 循环的使用小案例
```
# users = [{"name": "py01", "pwd": "123"},
#          {"name": "py03", "pwd": "123"},
#          {"name": "py04", "pwd": "123"}]
# # 需求查找用户 py03 是否在列表中
#
# for i in users:
#     if i['name']=='py03':
#         print(f'找到了这个用户{i}')
#         # continue
#         break
#     print(f'对比完用户{i}')
# else:
#     print('没有找到这个用户')

# for 循环的嵌套使用
'''
打印如下图形
1
12
123
1234
12345

*
* *
* * *
* * * *
'''
for i in range(1,6):
    for j in range(1,i+1): # 内层循环，根据行数打印数据 第一行打印1，第二行打印1 2 第三行打印 1 2 3...
        print(j,end='')
    print('')
for i in range(1,5):
    for j in range(1,i+1):
        print("* ",end='')
    print('')
```
# 函数
## 函数的定义和使用
```
'''
函数用法：
    函数名（）
函数的作用：对特定的一些功能进行封装，提高代码的重用率，进而提升开发的效率
函数的定义：
def 函数名（）：
    #函数体
函数名的命名规则：
    字母、下划线和数字组成，且不能用数字开头，不能使用python中的关键字
函数命名的风格：
    1、单词之间用下滑线隔开（python 中函数命名更推荐这种风格）
    2、大驼峰
    2、小驼峰
函数执行后，有没有数据是根据函数的返回值来决定的
'''
# 定义一个打印三角形的函数

def func():
    for i in range(1,6):
        for j in range(1,i+1):
            print(f'{j}',end='')
        print('')
func()
```
## 自定义函数的返回值
```
'''
函数的返回值：
    函数的返回值是有return 决定的
    函数中没有return 函数的返回值默认None
    return 后面没有内容，返回值也是None
    函数要返回多个值 return 后面每个数据用逗号隔开
return 作用
    1、返回数据
    2、终止函数，跳出函数
'''
def func():
    print('python')
    # return [666,777]
    return 'python'
res=func()
print(res)
```
## 函数的参数
```
'''
函数的参数：
    形参和实参
    定义的参数为形参
    调用实际传达的为实参
参数传递：
    调用函数的时候，参数传递的方式
    位置传参：通过位置传递参数（按照顺序传递，第一个实参传给第一个形参）
    关键字传参：通过参数名指定传给某个参数（传参的时间不注意参数的位置）
参数定义的方式
    1、必备参数(必须参数)：直接写个参数名
    2、缺省参数（默认参数）：定义的时候给参数设置一个默认值
    3、可变参数（动态参数，不定长参数）：*arg,**kwargs

'''
```
### 不定长参数
```
'''
不定长参数（动态参数，可变参数）
    *args  定义的时候参数名前加一个*(通常定义为*args),可以接受0个或多个位置传参，保存为一个元组
    **kwargs 定义的时候参数名前加两个**（通常定义为**kwargs）,可以接受0个或多个关键字传参，保存为一个字典

'''
def func(*args,**kwargs):
    print(args)
    print(type(args))
    print(kwargs)
    print(type(kwargs))
func(1,2)
func(a=1,b=2)

```
### 函数参数的拆包
```
def func(a,b):
    print('a=',a)
    print('b=',b)
# 传递参数时，可以用* 对列表，元组,集合拆包
# li=[11,22]
# func(*li)
# tu=(44,55)
# func(*tu)
# set1={66,77}
# func(*set1)
# 传递参数时，可以用** 对字典拆包
dic={"a":11,"b":22}
func(**dic)

tuple2=(1,2)
a,b=tuple2
print(a,type(a))
print(b,type(b))
```
## 函数的作用域
```angular2
''
全局变量：直接定义在py文件中的变量，叫全家变量，在该文件中任何地方都可以使用
局部变量：在函数内部定义的变量，叫局部变量，只能在该函数内部使用，函数外部无法使用
变量的查找过程：由内到外（先找自身，没有再去外部寻找）
如何再函数内部修改全局变量？
    global:再函数内部声明全局变量
'''

# 定义一个全局变量
name='daodao'

# def func():
#     # 定义一个局部变量
#     a=1
#     print(a)
#     print(name)
# func()
# print(name)

'''
变量的查找过程：由内到外（先找自身，没有再去外部寻找）
'''
# a=10
# def func():
#     #a=1
#     print(a)
# func()
'''
如何再函数内部修改全局变量？
    global:再函数内部声明全局变量
'''

a=10
def func():
    global a
    a+=1
    print(a)
func()
print(a)
```
# pythcarm 的debug调试
```angular2
'''
pycharm 自带的debug调试工具
F8：按一下 往下执行一行代码（遇到函数调用，直接运行函数的结果，不会进入函数内部）
F7：按一下 往下执行一行代码（遇到函数调用会进入内部）
'''
name = 'daodao'
age = 18
li1=[11,22,33]

def func():
    aa=100
    bb=200
    print(aa)
    print(bb)
print(name)
func()
```
# debug 定位问题
```
'''
print ：调试
打断点：断点打在报错的那一行上，或者之前都可以
'''
a=1
b='1'
def func():
    c=a+b
    print(c)
func()
```
## 内置函数
```angular2
'''
len() 获取 字符串\列表\元组\字典中的元素总数量（数据的长度）
max() 获取数据元素中的最大值
min() 获取数据元素中的最小值
sum() 对元素进行求和
eval() 识别字符串中的python表达式
zip() 聚合打包
'''
# len() 获取 字符串\列表\元组\字典中的元素总数量（数据的长度）
s1 = "asdfoifjashf"
print(len(s1))
li=[11,33,44]
print(len(li))
dic={'a':11,'b':22}
print(len(dic))

# max() 获取数据元素中的最大值
print(max(li))
# min() 获取数据元素中的最小值
print(min(li))
# sum() 对元素进行求和
print(sum(li))
# eval 识别字符串中的python表达式

li="[1,2,3]"
res=eval(li)  #==> [1,2,3]
print(res)
s2='print("python666")'
eval(s2)
# zip 聚合打包
li=['name','age','gender','666']
li2=['daodao',18,'男']
res = zip(li,li2)
# print(list(res))
# 把上面的元素转换成字典，li中元素作为键，li2中的 元素作为值
print(dict(res))
```
## 高级内置函数
```angular2
'''
enumerate 获取 字符串/列表/元组每个元素和对应的下表
filter 过滤器函数
匿名函数
'''
#enumerate 获取 字符串/列表/元组每个元素和对应的下表
s = "sfsdgfsk"
li2=enumerate(s)
print(list(li2))

# filter 过滤器函数
# 参数1：过滤的规则函数
# 参数2：要被过滤的数据

# 将case_id大于3的用例数据过滤出来
res1 = [
    {'case_id': 1, 'case_title': '用例1', 'url': 'www.baudi.com', 'data': '001', 'excepted': 'ok'},
    {'case_id': 4, 'case_title': '用例4', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 2, 'case_title': '用例2', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 3, 'case_title': '用例3', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 5, 'case_title': '用例5', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'}
]

def func(data):
    return data['case_id']>3
# 使用过滤器对数据进行过滤
res= list(filter(func,res1))
print(res)

# 匿名函数 
f=lambda x,y:x+y
print(f(1,3))

```
## 文件操作
```angular2
'''
文件操作
打开文件  open(参数1，参数2，参数3)
参数1：指定文件
参数2：打开的模式
    r: 读取模式，如果打开的文件不存在，直接报错
    a: 追加写入（在文件中原有的内容最后追加写入），被打开的文件不存在，会自动创建一个
    w: 覆盖写入（清空文件中原有的内容），被打开的文件不存在，会自动创建一个

    # 操作一下图片，视频等文件
    rb: 读取模式，如果打开的文件不存在，直接报错（以二进制模式去打开文件）
    ab: 追加写入（在文件中原有的内容最后追加写入），被打开的文件不存在，会自动创建一个（以二进制模式去打开文件）
    wb: 覆盖写入（清空文件中原有内容），被打开的文件不存在，会自动创建一个（以二进制模式去打开文件）
参数3：编码方式（"utf-8"）
'''
# 打开文件，返回一个操作的句柄
# f=open(file='text.txt',mode='r',encoding='utf-8')
# print(f)

#读取内容

# 第一种读取全部内容
# context=f.read()
# print(context)

# 第二种读取一行内容
# data=f.readline()
# print(data)

# 第三中把所有内容按行读取出来放在列表中
# data2=f.readlines()
# print(data2)

# 文件写入
# f=open('text.txt','a',encoding='utf-8')
# f.write('python')

# 以二进制的模式去打开文件
# f=open('微信图片_20210514104050.png','rb')
# print(f.read())
# 关闭文件
# f.close()

# with: 开启open返回文件句柄对象的上下文管理器（执行完with的代码语句后，会自动关闭文件）
with open('text.txt','r',encoding='utf-8') as f:
    c=f.read()
    print(c)

```
## python 的路径处理
```angular2
'''
os模块：python内置的模块
os.path.dirname:获取文件或目录，所在的父类目录路径
os.path.join: 路径拼接

魔法变量
1、__file__: 代表当前文件的绝对路径
2、__name__:
    如果当前文件是程序的启动文件，它的值是__main__
    如果不在启动文件中，代表的就是所在文件（模块）的模块名
'''
# 魔术变量的操作
# print('当前运行文件中打印__name__:',__name__)

import os

# 打印当前文件的绝对路径
print(__file__)
# 获取当前文件/目录所在的父级目录
dir=os.path.dirname(__file__)
print(dir)
# 获取项目目录路径
BASIDIR=os.path.dirname(dir)
print(BASIDIR)
# 路径拼接
file_path=os.path.join(BASIDIR,r'python\test02.py')
print(file_path)
```
## os 模块的其他用法
```angular2
import os
'''
linux:
os.getcwd(): pwd 获取当前的工作路径
os.chdir(): cd 切换工作路径
os.mkdir(): mkdir() 创建文件夹
os.mkdir(): rm 删除目录
os.listdir(): ls 获取当前工作路径下，所有的文件和目录

'''
# 获取当前的工作路径
print(os.getcwd())
# 切换工作路径
os.chdir('..')
print(os.getcwd())
# 创建文件夹
# os.mkdir('python666')
# 获取当前工作路径下，所有的文件和目录
print(os.listdir())
# 给定一个路径，判断是否是目录的路径
res=os.path.isdir(r'E:\python-automation\python_基础\day01')
print(res)
# 给定一个路径，判断是否是文件的路径
res2=os.path.isfile(r'E:\python-automation\python_基础\day01\task01.py')
print(res2
```
## 异常处理
```angular2
'''
try:
    不可控的因素造成的错误，需要用try来进行捕获
    #用户输入
    # 打开文件，文件不存在
    # 发送网络请求时，网络超时

except:

else:

finally:
'''

try:
    # try 下面写有可能出现异常的代码
    score=int(input("请输入成绩："))
except:
    # 处理异常之后，进行代码处理
    print('输入的数据不符合规范，默认给分0')
    score=0
else:
    # 代码没有出现异常，执行else中的代码
    print('代码没有出现异常，执行else')
finally:
    # 不管代码有没有异常，都会去执行代码
    print('finally不管代码有没有异常都会执行')

if score>60:
    print('及格')
else:
    print('不及格{}'.format(score))
```
## 异常捕获处理
```angular2
'''
except 可以指定捕获异常类
assert 断言  比较两个数据是否一致
raise 抛出异常，打印到控制台
'''
# 捕获单个异常类型
# try:
#     print(a)
# except NameError:
#     print('捕获到了异常')

# 捕获多个异常类型(不同的异常类型需要做不同的处理时候)

# try:
#     print(a)
#     int('a')
# except NameError as e:
#     print(f'捕获异常{e}')
# except ValueError as e2:
#     print(f'捕获异常{e2}')

# 捕获多个异常类型（不同的异常类型，做统一处理的时候）
# try:
#     print(a)
#     int('a')
# except (NameError,ValueError) as e:
#     print(f'捕获异常{e}')

# 捕获所有异常类型

# try:
#     int('a')
#     print(a)
# except:
#     print('捕获异常')

# 小案例

result='8888'
expected='888'

try:
    assert  result ==expected
except AssertionError as e:
    print('用例未通过')
    raise e

```
## 面向对象
```angular2
'''
面向对象
int(类)：1，2，3...
str（类）：’ab','a'
list(类）：[1,3]

类（类型）和对象（这个类型中的一个具体的数据）
类的定义：
class 类名():
    pass
类名的规范：大驼峰的形式（每一个单词第一个字母大写）

通过类去创建对象
类名()
'''

class GirlFriend():
    pass

obj1=GirlFriend()
print(obj1)
```
## 类属性和对象属性
```angular2
'''
类里面应该有该类事务共同的特征和行为
特征：属性
行为：方法

类属性：这个类所有的对象都有这个属性，属性值是一样的
类属性的定义：直接定义在类里面的变量，类属性
类属性值的获取：可以通过对象来获取，也可以用来来获取
获取类属性值： 类名.属性名 对象名.属性名

对象属性（实现属性）：这个类所有的对象都可以有这个属性，每个对象的属性值可以不一样
实例属性的定义：对象名.属性名=属性值
实例属性值的获取：对象名.属性名

方法：定义在类里的函数
__init__ 方法 初始化方法（初始化对象的属性） 创建对象的同时添加对象属性
'''
class GirlFriend:
    '''
    女朋友类
    '''
    gender='女'
    def __init__(self,name,face,hieght,leg):
        self.name=name
        self.face=face
        self.height=hieght
        self.leg=leg
obj1=GirlFriend('球总监','球脸',176.5,'两米')
obj2=GirlFriend('小amy','amy脸',160,'一米')
obj3=GirlFriend('好人','好人脸',176,'一米')
obj4=GirlFriend('拆拆','拆拆脸',176,'一米')
obj5=GirlFriend('华健','贱脸',190,'一米')
# print(obj1.__dict__)
# print(obj2.__dict__)
# print(obj3.__dict__)
# print(obj4.__dict__)
# print(obj5.__dict__)

# 获取属性值
## 类属性值的获取:可以通过对象来获取，也可以通过类获取
# print(obj1.gender)
# print(GirlFriend.gender)
## 对象（实例）属性值的获取：只能通过对象来获取
print(obj1.name)
```
## self 和方法
```angular2
'''
方法：定义在类中的函数
方法的调用：对象名.方法名()
self: 代表的是对象本身，哪个对象去调用方法，那么self就代表的是那个对象

'''
class GirlFriend():
    gender='女'
    def __init__(self,name,face,height,leg):
        self.name=name
        self.face=face
        self.height=height
        self.leg=leg

    def skill(self):
        print(f'{self.name}会逛街')

obj1=GirlFriend('小花','好看',170,'一米')
obj2=GirlFriend('小红','好看',170,'一米')
obj1.skill()
obj2.skill()
```
##  类方法和实例方法
```angular2
'''
对象方法（实例方法）：
实例方法的定义：直接定义在类中的函数  第一个参数是self,self代表的是对象本身
实例方法的调用： 对象名.方法（）

类方法：
类方法定义：要通过classmethod装饰器来声明一个类方法
第一个参数是cls,cls代表的是类本身
类方法的调用：类名.方法（） 或 对象名.方法（）

'''
class GirlFriend():
    gender='女'
    def __init__(self,name,face,height,leg):
        self.name=name
        self.face=face
        self.height=height
        self.leg=leg

    def skill(self):
        print(f'{self.name}会逛街')

    @classmethod
    def skill2(self):
        print('看电视')

obj1=GirlFriend('小花','好看',170,'一米')

#  类方法的调用
obj1.skill2() # 实例对象.方法()
GirlFriend.skill2() # 类名.方法（）
# 实例对象的调用
obj1.skill()
# GirlFriend.skill() #TypeError: skill() missing 1 required positional argument: 'self'  类不能调用实例对象的方法
```
## 类方法和静态方法
```angular2
'''
类里面的属性和方法
属性：
    类属性：直接定义在类里的变量，叫做类属性，类属性可以通过类访问，可以通过实例对象访问
          公有属性：不管类里面还是类外面都可以访问
         私有属性：两个下划线开头的属性叫做私有属性，只能在类里面访问，在类外部是无法使用的

    实例属性：
        实例属性的定义：对象.属性名=属性值
        实例属性只能通过对象访问
方法：
    实例方法：
    第一个参数是self,self代表实例对象本身
    只能使用实例对象调用
    实例方法一般是以实例对象为主体去调用的

类方法：
    第一个参数是cls,self 代表类的本身
    可以实例对象调用，也可以类调用
    类的方法一般以类为主体调用的
静态方法：
    没有必须要定义的参数
    可以类调用，也可实例对象调用
    静态方法调用的时候，内部不会使用到对像和类的相关属性。
'''
import random

class MyClass():

    attr=100
    __attr=200

    @classmethod
    def cls_func(self):
        print(f'{self}类方法')

    def func(self):
        phone=self.random_phone()
        print(f'{MyClass.__attr}收机号码{phone}')

    @staticmethod
    def random_phone():
        phone='13'
        for i in range(9):
            phone+=str(random.randint(0,9))
        return phone

# print(MyClass.attr)
# print(MyClass.__attr) #类外面无法访问私有属性
m=MyClass()
m.func()
m.cls_func()
MyClass.cls_func()

```
## 类的继承
```angular2
'''

class  BassClass(object):
    money=10
    def func(self):
        print('赚钱的方法，一次赚1000万')

class ClassV1(BassClass):
    # 通过继承可以获得父类的方法和属性
    pass

c=ClassV1()
print(c.money)
c.func()
```
## 继承和重写父类方法小案例
```angular2
'''
继承的作用，提高代码的重用率
重写父类方法：在子类定义一个和父类相同的方法，这个操作叫重写父类方法
在子类中调用父类同名方法：
方式一：父类名.方法名（self）
方式二：super().方法名(self)
推荐使用：方式二
'''

# 需求一，大哥大收机(1995)
class BasePhone():

    def call_phone(self):
        print('这是拨打语音电话的功能')

# 需求二，功能机（2005）
class PhoneV1(BasePhone):
    def play_music(self):
        print('播放音乐')

    def send_msg(self):
        print('发送信息')

# 需求三：智能机（2019）
class PhoneV2(PhoneV1):

    # 重写父类方法
    def call_phone(self):
        print('拨打视频电话')
        print('视频通话5分钟后，切换语音通话')
        super().call_phone() #调用父类同名方法

    def game(self):
        print('打游戏')

p=PhoneV2()
p.call_phone()
p.play_music()
p.send_msg()
p.game()
```
## 动态属性设置
```angular2
'''
对象的__dict__属性：获取对象的所以属性以字典形式返回
动态属性设置：
内置函数setattr(参数1，参数2，参数3)
参数1：对象
参数2：给对象设置属性名（字符串类型）
参数3:属性值
'''

# class Cases():
#     def __init__(self,case_id,title,url,data):
#         self.case_id=case_id
#         self.title=title
#         self.url=url
#         self.data=data
# data = {'case_id': 1, 'title': '用例1', 'url': 'www.baudi.com', 'data': '001'}
# # 需求字典中的key为属性，value为属性值
# case= Cases(**data)
# print(case.__dict__)

# setattr 动态属性设置
data = {'case_id': 1, 'title': '用例1', 'url': 'www.baudi.com', 'data': '001'}
class MyClass():
    '''定义一个类'''
    pass

m=MyClass()
# setattr(m,'name','daodao')
# print(m)
# print(m.name)
# print(MyClass.__dict__)
# print(m.__dict__)

# for i in range(1,11):
#     setattr(m,f'data{i}',i)
# print(m)
# print(m.__dict__)
# print(MyClass.__dict__)

for k,v in data.items():
    setattr(m,k,v)
print(m)
print(m.__dict__)

# 获取属性值
values=getattr(m,"case_id")
print(values)
#删除属性值
delattr(m,'case_id')

values=getattr(m,"case_id")
```
# 单元测试
```angular2
''
这个文件是测试程序运行的启动文件
'''
import unittest

# 第一步创建一个测试套件
suite=unittest.TestSuite()

# 第二步将测试用例加载到测试套件中

## 第1种通过模块去加载
# from 单元测试.day13 import testcase
# loader=unittest.TestLoader()  # 创建一个加载对象
# suite.addTest(loader.loadTestsFromModule(testcase))
## 第2种通过测试用例类去加载
# from 单元测试.day13 import testcase
# loader=unittest.TestLoader()
# suite.addTest(loader.loadTestsFromTestCase(testcase.LoginTestCase))

## 第3种 添加单条测试用例
# from 单元测试.day13.testcase import LoginTestCase
# case=LoginTestCase("test_login_pass") #通过测试用例类创建测用例试对象时，需要传入测试用例的方法名(字符串)

## 第4种 通过路径加载
loader=unittest.TestLoader()
suite.addTest(loader.discover(r"E:\python-automation\单元测试\day13"))
# 第三步创建一个测试运行程序启动器
runner=unittest.TextTestRunner()

# 第四步使用启动器去执行测试套件
runner.run(suite)
```
## HTMLTestRunner
```angular2
import unittest
from unit_learn.day13 import testcases
from unit_learn.day13.HTMLTestRunnerNew import  HTMLTestRunner

# 第一步 创建一个测试套件
suite=unittest.TestSuite()

# 第二步 讲测试用例加载进测试套件，按照模块加载
loader=unittest.TestLoader()
suite.addTest(loader.loadTestsFromModule(testcases))

# 第三步 创建一个测试运行程序启动器
runner=HTMLTestRunner( stream=open('report.html','wb'),
                       title="python单元测试报告",
                       description="python单元测试报告，用HTMLTestRunner生成测试报告",
                       tester="daodao,小明，大哥")
# 第四步 使用启动器去启动测试套件
runner.run(suite)
```
# 操作excel 
## openpyxl的使用
```angular2
import openpyxl

# 第一步打开工作簿
workbook=openpyxl.load_workbook(r'E:\python-automation\unit_learn\day14\project_14day_v1\cases.xlsx')

# 第二步 选择表单对象
sheet=workbook["login"]

# 第三步 通过表单对象选中表格读取数据
##  1、读取内容
data=sheet.cell(row=5,column=1)
# print(data.value)
## 2、写入内容
sheet.cell(row=7,column=3,value='(",")')

## 写入内容后一定要保存才能生效
workbook.save(r'E:\python-automation\unit_learn\day14\project_14day_v1\cases.xlsx')

## 获取最大行和最大列
print(sheet.max_row)
print(sheet.max_column)
# 注意点不要随意在表格中敲空格
# rows 按行获取格子对象，每一行的格子对象放入一个元组中
print(list(sheet.rows))
```
## 读取excel是数据
```angular2
import openpyxl

# 打开工作簿
workbook=openpyxl.load_workbook('cases.xlsx')
# 选中表单
sheet=workbook['register']
# 按行获取格子对象，每行对象放在元组中
rows=list(sheet.rows)
title=[]
for i in rows[0]:
    title.append(i.value)
cases=[]
for row in rows[1:]:
    data = []
    for r in row:
        data.append(r.value)
    case=dict(zip(title,data))
    cases.append(case)
print(cases)

tu=(11,222,333 ,44)
k,v,c,g=tu
print(k)
print(v)

```
## 配置文件的操作
```angular2
'''
创建对象
读取文件
get获取内容
getint 读取int型数据
getfloat 获取float型的数据
getboolean 获取boolean型的数据
set 修改配置文件中的值
write 写入文件
'''
from configparser import ConfigParser

conf=ConfigParser()
c=conf.read('conf.ini',encoding='utf-8')
res=conf.get('logging','level')
print(res)
res2=conf.getint('daodao','age')
print(res2,type(res2))
res3=conf.getfloat('daodao','money')
print(res3,type(res3))
res4=conf.getboolean('daodao','switch')
print(res4,type(res4))
conf.set('daodao','aaa','python666')
conf.write(open('conf.ini','w',encoding='utf-8'))
```
## requests
```angular2
'''
requests
get
post
json类型数据
表单类型数据
上传文件


json()
text()
content.decode()

'''
import requests

url="http://8.129.91.152:8766/futureloan/member/register"
register_url='http://api.lemonban.com/futureloan/member/register'

header={"X-Lemonban-Media-Type": "lemonban.v1",
        "Content-Type": "application/json"}
data={
    "mobile_phone": "18888888844",
    "pwd": "12345678",
    "reg_name": "daodao",
    "type": 0
}

# json类型的数据
# response=requests.post(url=register_url,json=data,headers=header)
# print(response.json())
# print(response.text)
# print(response.content.decode('utf-8'))
# 表单类型的数据
response=requests.post(url=register_url,data=data,headers=header)
print(response.text)
# 上传文件
# response=requests.post(url=register_url,files=None)
# print(response.content.decode('utf-8'))
# print(type(response.json()))

# get请求
# get_url="http://test.lemonban.com/futureloan/mvc/api/member/register"
# get_data = {
#     "mobilephone": "18888888884",
#     "pwd":"123qwe"
# }
# response = requests.get(url=get_url, params=get_data)
# print(response.json())

# patch
# patch_url='http://api.lemonban.com/futureloan/member/update'
# patch_headers = {
#     "X-Lemonban-Media-Type": "lemonban.v1",
#     "Content-Type": "application/json"
# }
# patch_data = {
#     "member_id": 1234599691,
#     "reg_name": "daodao888"
# }
#
# response=requests.patch(url=patch_url,json=patch_data,headers=patch_headers)
# print(response.json())
```
## 使用jsonpath提取数据
```angular2
import requests
import jsonpath

login_url="http://api.lemonban.com/futureloan/member/login"

header = {
    "X-Lemonban-Media-Type": "lemonban.v2",
    "Content-Type": "application/json"
}

login_data={
    "mobile_phone": "18888888844",
    "pwd": "12345678"
}
response=requests.post(url=login_url,json=login_data,headers=header)
json_data=response.json()

member_id=jsonpath.jsonpath(json_data,'$..id')[0]
type_token=jsonpath.jsonpath(json_data,'$..token_type')[0]
token=jsonpath.jsonpath(json_data,'$..token')[0]
token_data=type_token +' ' + token
header_token = {
    "X-Lemonban-Media-Type": "lemonban.v2",
    "Content-Type": "application/json",
    "Authorization":token_data
}

recharge_data = {
    "member_id":member_id,
    "amount":2000
}
recharge_url="http://api.lemonban.com/futureloan/member/recharge"
response=requests.post(url=recharge_url,json=recharge_data,headers=header_token)
print(response.json())
```
## json 类型转换
```angular2
import json
data = {"name":"musen","id":18,"msg":None}
json_data = '{"name":"musen","id":19,"msg":null}'
# json.loads  将字符串转化成python类型，null转化成None
res=json.loads(json_data)
print(res,type(res))
# json.dumps 将python类型转化成字符串，None转化成null
res=json.dumps(data)
print(res,type(res))
```
## 通过cookie 和session 鉴权的接口处理方式
```angular2
import requests

#  --------------------------------使用requests直接发生请求，无法通过session鉴权--------------------------------------
# 老版接口
# register_url = "http://test.lemonban.com/futureloan/mvc/api/member/register"
# data = {
#     "mobilephone": "18888888885",
#     "pwd":"12345678"
# }
# response = requests.get(url=register_url, params=data)
# print(response.json())

# login_url="http://test.lemonban.com/futureloan/mvc/api/member/login"
# data = {
#     "mobilephone": "18888888885",
#     "pwd":"12345678"
# }
# res=requests.post(login_url,data)
# print(res.json())
# print(res.cookies)

# recharge_url="http://test.lemonban.com/futureloan/mvc/api/member/recharge"
# data = {
#     "mobilephone": "18888888885",
#     "amount":"2000"
# }
# res=requests.post(recharge_url,data)
# print(res.json())
# print(res.cookies)

#-------------------使用requests模块中的session对象发送请求----------------------------
# session()能自动记录上一次的cookie信息
se=requests.session()
login_url="http://test.lemonban.com/futureloan/mvc/api/member/login"
data = {
    "mobilephone": "18888888885",
    "pwd":"12345678"
}
res=se.post(login_url,data)
print(res.json())
print(res.cookies)

recharge_url="http://test.lemonban.com/futureloan/mvc/api/member/recharge"
data = {
    "mobilephone": "18888888885",
    "amount":"2000"
}
res=se.post(recharge_url,data)
print(res.json())
print(res.cookies)
```
## requests请求返回数据的提取
```angular2
import requests
import time
# login_url="http://api.lemonban.com/futureloan/member/login"
#
# header = {
#     "X-Lemonban-Media-Type": "lemonban.v2",
#     "Content-Type": "application/json"
# }
#
# login_data={
#     "mobile_phone": "18888888844",
#     "pwd": "12345678"
# }
# response=requests.post(url=login_url,json=login_data,headers=header)
# json_data=response.json()
# member_id=json_data['data']['id']
# type_token=json_data['data']['token_info']['token_type']
# token=json_data['data']['token_info']['token']
# token_data=type_token +' '+ token
#
# header_token = {
#     "X-Lemonban-Media-Type": "lemonban.v2",
#     "Content-Type": "application/json",
#     "Authorization":token_data
# }
#
# recharge_data = {
#     "member_id":member_id,
#     "amount":2000
# }
# recharge_url="http://api.lemonban.com/futureloan/member/recharge"
# response=requests.post(url=recharge_url,json=recharge_data,headers=header_token)
# print(response.json())

login_url="http://api.lemonban.com/futureloan/member/login"
header = {
    "X-Lemonban-Media-Type": "lemonban.v3",
    "Content-Type": "application/json"
}

login_data={
    "mobile_phone": "18888888844",
    "pwd": "12345678"
}
response=requests.post(url=login_url,json=login_data,headers=header)
json_data=response.json()
member_id=json_data['data']['id']
timestamp=int(time.time())
type_token=json_data['data']['token_info']['token_type']
token=json_data['data']['token_info']['token']
stimestamp_token_sign=token[:50]+ str(timestamp)
Bearer_token=type_token + ' ' + token
header_token = {
    "X-Lemonban-Media-Type": "lemonban.v3",
    "Content-Type": "application/json",
    "Authorization":Bearer_token
}
recharge_data = {
    "member_id":member_id,
    "amount":2000,
    "sign":stimestamp_token_sign
}
recharge_url="http://api.lemonban.com/futureloan/member/recharge"
response=requests.post(url=recharge_url,json=recharge_data,headers=header_token)
print(response.json())

import base64
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher_pkcs1_v1_5

def rsaEncrypt(msg):
    """
    公钥加密
    :param msg: 要加密内容
    :type msg:str
    :return: 加密之后的密文
    """
    key = '''-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQENQujkLfZfc5Tu9Z1LprzedE
O3F7gs+7bzrgPsMl29LX8UoPYvIG8C604CprBQ4FkfnJpnhWu2lvUB0WZyLq6sBr
tuPorOc42+gLnFfyhJAwdZB6SqWfDg7bW+jNe5Ki1DtU7z8uF6Gx+blEMGo8Dg+S
kKlZFc8Br7SHtbL2tQIDAQAB
-----END PUBLIC KEY-----'''
    publickey = RSA.importKey(key)
    cipher = Cipher_pkcs1_v1_5.new(publickey)
    # 分段加密
    cipher_text = []
    for i in range(0, len(msg), 80):
        cont = msg[i:i + 80]
        cipher_text.append(cipher.encrypt(cont.encode()))
    # base64 进行编码
    cipher_text = b''.join(cipher_text)
    cipher_result = base64.b64encode(cipher_text)
    # 返回密文
    return cipher_result.decode()
if __name__ == '__main__':
    # 待加密内容
    # pwd = "123qwe"
    # 加密操作
    en_msg = rsaEncrypt(msg=stimestamp_token_sign)
    print('加密密文：', en_msg)
```
## 使用jsonpath提取数据
```angular2
import requests
import jsonpath

login_url="http://api.lemonban.com/futureloan/member/login"

header = {
    "X-Lemonban-Media-Type": "lemonban.v2",
    "Content-Type": "application/json"
}

login_data={
    "mobile_phone": "18888888844",
    "pwd": "12345678"
}
response=requests.post(url=login_url,json=login_data,headers=header)
json_data=response.json()

member_id=jsonpath.jsonpath(json_data,'$..id')[0]
type_token=jsonpath.jsonpath(json_data,'$..token_type')[0]
token=jsonpath.jsonpath(json_data,'$..token')[0]
token_data=type_token +' ' + token
header_token = {
    "X-Lemonban-Media-Type": "lemonban.v2",
    "Content-Type": "application/json",
    "Authorization":token_data
}

recharge_data = {
    "member_id":member_id,
    "amount":2000
}
recharge_url="http://api.lemonban.com/futureloan/member/recharge"
response=requests.post(url=recharge_url,json=recharge_data,headers=header_token)
print(response.json())
```
## 通过cookie和session鉴权的接口处理方式
```angular2
import requests

#  --------------------------------使用requests直接发生请求，无法通过session鉴权--------------------------------------
# 老版接口
# register_url = "http://test.lemonban.com/futureloan/mvc/api/member/register"
# data = {
#     "mobilephone": "18888888885",
#     "pwd":"12345678"
# }
# response = requests.get(url=register_url, params=data)
# print(response.json())

# login_url="http://test.lemonban.com/futureloan/mvc/api/member/login"
# data = {
#     "mobilephone": "18888888885",
#     "pwd":"12345678"
# }
# res=requests.post(login_url,data)
# print(res.json())
# print(res.cookies)

# recharge_url="http://test.lemonban.com/futureloan/mvc/api/member/recharge"
# data = {
#     "mobilephone": "18888888885",
#     "amount":"2000"
# }
# res=requests.post(recharge_url,data)
# print(res.json())
# print(res.cookies)

#-------------------使用requests模块中的session对象发送请求----------------------------
# session()能自动记录上一次的cookie信息
se=requests.session()
login_url="http://test.lemonban.com/futureloan/mvc/api/member/login"
data = {
    "mobilephone": "18888888885",
    "pwd":"12345678"
}
res=se.post(login_url,data)
print(res.json())
print(res.cookies)

recharge_url="http://test.lemonban.com/futureloan/mvc/api/member/recharge"
data = {
    "mobilephone": "18888888885",
    "amount":"2000"
}
res=se.post(recharge_url,data)
print(res.json())
print(res.cookies)
```
