"""
============================
Author:dao dao
Time:2021/6/20
E-mail:402583874@qq.com
============================
"""
import unittest
import unittestreport

suite = unittest.defaultTestLoader.discover('.')
runner = unittestreport.TestRunner(suite,
                                   filename="report.html",
                                   report_dir=".",
                                   title='测试报告',
                                   tester='测试员',
                                   desc="XX项目测试生成的报告",
                                   templates=1
                                   )
runner.run()
