"""
============================
Author:dao dao
Time:2021/6/20
E-mail:402583874@qq.com
============================
"""
import unittest
import openpyxl
import requests
import logging
from unittestreport import list_data, ddt


@ddt
class TestLogin(unittest.TestCase):
    wb = openpyxl.load_workbook('apicases.xlsx')
    sh = wb['login']
    rows = list(sh.rows)
    title = [i.value for i in rows[0]]
    cases = []
    for row in rows[1:]:
        data = []
        for i in row:
            data.append(i.value)
        case = dict(zip(title, data))
        cases.append(case)

    @list_data(cases)
    def test_login(self, case):
        url = 'http://api.lemonban.com/futureloan' + case['url']
        headers = {"X-Lemonban-Media-Type": "lemonban.v2"}
        data = eval(case['data'])
        row = case['case_id'] + 1
        expected = eval(case['expected'])
        response = requests.post(url=url, json=data, headers=headers)
        res = response.json()
        try:
            self.assertEqual(expected.get('code'), res.get('code'))
            self.assertEqual(expected.get('msg'), res.get('msg'))
        except AssertionError as e:
            self.sh.cell(row=row, column=8, value='失败')
            self.wb.save('apicases.xlsx')
            self.mylog().info(f'用例{case["title"]}--------执行失败')
            raise e
        else:
            self.sh.cell(row=row, column=8, value='成功')
            self.wb.save('apicases.xlsx')
            self.mylog().info(f'用例{case["title"]}--------执行成功')

    @staticmethod
    def mylog():
        mylog = logging.getLogger('dao dao')
        mylog.setLevel('DEBUG')
        fh = logging.FileHandler('test.log', encoding='utf-8')
        fh.setLevel('DEBUG')
        mylog.addHandler(fh)
        formatter = logging.Formatter('%(asctime)s - [%(filename)s-->line:%(lineno)d] - %(levelname)s: %(message)s')
        fh.setFormatter(formatter)
        return mylog


if __name__ == '__main__':
    suite=unittest.TestSuite()
    suite.addTest(TestLogin('test_login'))
    runner=unittest.TextTestRunner()
    runner.run(suite)