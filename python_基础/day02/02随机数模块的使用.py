# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 14:43
# @Author  : daodao
# @FileName: 02随机数模块的使用.py
# @Software: PyCharm
# @qq ：402583874

#导入随机数模块
import random

# 指定范围生成一个随机整数（包含边界）
num= random.randint(1,3)
print(num)
# 随机生成一个0-1之间的小数p [左闭右开 边界值包含0不包含1)
num1=random.random()
print(num1)

# 如何生成10到20 之间的小数
num2=random.randint(10,20)
num3=random.random()
num4=num2+num3
print(num4)
# 源码查看方式： ctrl + 鼠标左键点击

# 解决浮点运算的精度问题

import  decimal
a=2.89
b=0.3
print(a-b)
aa=decimal.Decimal('2.89')
bb=decimal.Decimal('0.3')
print(aa-bb)


