# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 15:44
# @Author  : daodao
# @FileName: task.02.py
# @Software: PyCharm
# @qq ：402583874
"""
作业
1、用户输入一个数值，请判断用户输入的是否为偶数？是偶数输出True,不是输出False
（提示:input输入的不管是什么，都会被转换成字符串，自己扩展，想办法转换为数值类型，再做判段，）

2、卖橘子的计算器：写一段代码，提示用户输入橘子的价格，然后随机生成购买的斤数（5到10斤之间）
，最后计算出应该支付的金额！

3、现在有变量a = 'hello',b='python',c='!'
通过相关操作转换成字符串：'hello python !'（注意点:转换之后单词之间有空格）

4、随机生成一个130开头的手机号码

"""
# print('-------------第一题-------------------')
# num=int(input('请输入一个数：'))
# print(num%2==0) # 运算符比较，打印结果

# print('-------------第二题-------------------')
# import random
# price=float(input('请输入橘子的价格：'))
# num=random.randint(5,10)
# print('购买斤数：',num)
# res= price * num
# print('支付金额：',res)
# print('-------------第三题-------------------')
# #
# # a = 'hello'
# # b='python'
# # c='!'
# # str1=''.join((a,' ',b,' ',c))
# # print(str1)
print('-------------第四题-------------------')
str1='130'
import  random
end1=random.randint(100000000,999999999)
res=str1+str(end1)[1:]
print(res)