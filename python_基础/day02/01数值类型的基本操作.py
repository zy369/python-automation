# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 14:09
# @Author  : daodao
# @FileName: 01数值类型的基本操作.py
# @Software: PyCharm
# @qq ：402583874

'''
数值类型的数据
整数：int（整数型）
小数：float(浮点型)
布尔值：bool（一种特殊的数据类型）只要True 和 False两个值
复数 不学
type: 内置函数，用来查看数据类型
'''
# 整数
# a=20
# print(type(a))
# # 浮点数
# b=1.2
# print(type(b))
# # 布尔类型
# t=True
# print(t,type(t))

# python 中的运算符
## 算术运算符
"""
+ ：加
- ：减
* ：乘
/ ：除
** ：幂运算
% ：取余
// ：除法取整
"""
# a=20
# b=5
# res = a/b
# print(res)
# print(2**4)
# print(9//5)

# 赋值运算符     =   +=  -=  *=  /=  %=
# a=22
# a+=1 # a=a+1
# print(a)
#
#比较运算符  比较条件的成立，返回的是True , 不成立返回的是 False
'''
==  表示等于
！=  表示不等于
'''
# a=10
# b=10
# print(a==b)
# print(a>b)

# 逻辑运算符： 与 或 非
'''
and  真真为真（所有的条件都要成立，才会返回True,否则返回False）
or : 一真为真，假假为假 （只要一个条件成立，就返回True,否则返回False）
not: 取反
'''
print(10<8 and 9<10)
print(10<8 or 9<10)
print(not 10<7)

# 运算符的优先级 ： 用括号解决
print(10>8 and (11>9 or 11<19 ))
