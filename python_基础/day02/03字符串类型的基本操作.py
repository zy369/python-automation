# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 15:06
# @Author  : daodao
# @FileName: 03字符串类型的基本操作.py
# @Software: PyCharm
# @qq ：402583874

'''
字符串的定义： 通过引号（单引号、双引号、三引号）
注意点：
    空字符：
    空白字符：

'''
# 空字符：字符中没有任何内容
s1=''
print(s1)
# 空白字符：字符中有内容,内容是空格键，tab键等
s2=' '
print(s2)
s3= '   '
print(s3)
# python中任何数据类型都可以转换成bool值
#数据中有内容，转换成bool值后为：True
#数据中没有内容，转换成bool值后为：False
print(bool(s1))
print(bool(s2))
print(bool(s3))
print(type(s1))
print(type(s2))
print(type(s3))

num=999
print(type(999))
# 数值类型转换成字符串
s4=str(num)
print(s4,type(s4))

#字符串转换成数值
s='555'
num=int(s)
print(num,type(num))

# 下标索引取值
# 字符串的下标，从0开始（从前往后），从-1开始（从后往前）
str1='python hello'
res=str1[-4]
print(res)

# 字符串的切片操作
'''
[起始位置：终止位置] ： 取头不取尾，从起始位置开始，到终止位置前一个
起始位置： 不写，默认从头开始
终止位置： 不写，默认到结束
[起始位置：终止位置：步长] ：步长为多少，就是多少个取一个
'''
res=str1[:7]
print(res)
str2='12345678'
res=str2[::2]
# 12 34 56 78 9
print(res)
res=str2[::3]
#123 456 789
print(res)

#字符串拼接
desc1='我的名字叫daodao'
desc2='今年年龄是18岁'
# 第一种 +
res=desc1+desc2
print(res)
'''
# 第二种：join 方法
#   1、字符串X.join((字符串1，字符串2，字符串3，。。。))
    2、字符串1 字符串X 字符串2 字符串X 字符串3 字符串X
    
'''
res2=','.join((desc1,desc2,'python666'))
print(res2)