# -*- coding: utf-8 -*-
# @Time    : 2021/6/1 9:34
# @Author  : daodao
# @FileName: task09.py
# @Software: PyCharm
# @qq ：402583874

'''
异常处理

第一题
1、写出异常处理语句中try作用是什么，except,else,finally下面的代码分别在什么时候会执行？（简答题）
​
第二题
2、改善上节课的注册程序，打开文件的读取数据的时候，如果文件不存在会报错，请通过try-except来捕获这个错误，进行处理

第三题
3、优化之前作业的石头剪刀布游戏，用户输入时，如果输入非数字会引发异常，请通过异常捕获来处理这个问题。

'''
# 第一题
'''
1、try的作用
   try 可以用来监测代码是否出现异常（把有可能出现异常的代码放在try中）
2、except下面的代码什么时候执行
    try 中的代码出现异常，被except成功捕获后，会执行except中的代码
3、else下面的代码什么时候执行
    try 中的代码没有出现异常，执行else中的代码
4、finally下面的代码什么时候执行
    不管try中的代码有没有异常，finally下面的代码都会执行
'''
# 第二题
def work2():
    try:
        f=open('users.txt','a',encoding='utf-8')
        data=f.read()
        users=eval(data)
    except:
        users=[]
     # 注册功能代码（上次作业写的，不需要改动）
        username = input('请输入新账号:')  # 输入账号
        password1 = input('请输入密码：')  # 输入密码
        password2 = input('请再次确认密码：')  # 再次确认密码

        for user in users:  # 遍历出所有账号，判断账号是否存在
            if username == user['name']:
                print('该账户已存在')  # 账号存在，
                break
        else:
            # 判断两次密码是否一致
            if password1 != password2:
                print('注册失败，两次输入的密码不一致')
            else:
                # 账号不存在 密码一样，则添加到账户列表中
                users.append({'name': username, 'pwd': password2})
                print('注册成功！')
        f.write(str(users))

work2()