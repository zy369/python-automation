# -*- coding: utf-8 -*-
# @Time    : 2021/5/31 17:16
# @Author  : daodao
# @FileName: 03异常处理.py
# @Software: PyCharm
# @qq ：402583874
'''
try:
    不可控的因素造成的错误，需要用try来进行捕获
    #用户输入
    # 打开文件，文件不存在
    # 发送网络请求时，网络超时

except:

else:

finally:
'''

try:
    # try 下面写有可能出现异常的代码
    score=int(input("请输入成绩："))
except:
    # 处理异常之后，进行代码处理
    print('输入的数据不符合规范，默认给分0')
    score=0
else:
    # 代码没有出现异常，执行else中的代码
    print('代码没有出现异常，执行else')
finally:
    # 不管代码有没有异常，都会去执行代码
    print('finally不管代码有没有异常都会执行')

if score>60:
    print('及格')
else:
    print('不及格{}'.format(score))