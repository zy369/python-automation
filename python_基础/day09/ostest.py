# -*- coding: utf-8 -*-
# @Time    : 2021/5/31 17:26
# @Author  : daodao
# @FileName: ostest.py
# @Software: PyCharm
# @qq ：402583874

def func1():
    print('ostest中打印的__name__',__name__)


# 只有直接运行这个文件的时候，下面这个条件才会成立
if __name__ == '__main__':
    func1()
    print(999)