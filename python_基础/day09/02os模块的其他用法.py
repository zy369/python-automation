# -*- coding: utf-8 -*-
# @Time    : 2021/5/31 17:05
# @Author  : daodao
# @FileName: 02os模块的其他用法.py
# @Software: PyCharm
# @qq ：402583874

import os
'''
linux:
os.getcwd(): pwd 获取当前的工作路径
os.chdir(): cd 切换工作路径
os.mkdir(): mkdir() 创建文件夹
os.mkdir(): rm 删除目录
os.listdir(): ls 获取当前工作路径下，所有的文件和目录

'''
# 获取当前的工作路径
print(os.getcwd())
# 切换工作路径
os.chdir('..')
print(os.getcwd())
# 创建文件夹
# os.mkdir('python666')
# 获取当前工作路径下，所有的文件和目录
print(os.listdir())
# 给定一个路径，判断是否是目录的路径
res=os.path.isdir(r'E:\python-automation\python_基础\day01')
print(res)
# 给定一个路径，判断是否是文件的路径
res2=os.path.isfile(r'E:\python-automation\python_基础\day01\task01.py')
print(res2)