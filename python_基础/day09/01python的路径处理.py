# -*- coding: utf-8 -*-
# @Time    : 2021/5/31 16:05
# @Author  : daodao
# @FileName: 01python的路径处理.py
# @Software: PyCharm
# @qq ：402583874

'''
os模块：python内置的模块
os.path.dirname:获取文件或目录，所在的父类目录路径
os.path.join: 路径拼接

魔法变量
1、__file__: 代表当前文件的绝对路径
2、__name__:
    如果当前文件是程序的启动文件，它的值是__main__
    如果不在启动文件中，代表的就是所在文件（模块）的模块名
'''
# 魔术变量的操作
# print('当前运行文件中打印__name__:',__name__)

import os

# 打印当前文件的绝对路径
print(__file__)
# 获取当前文件/目录所在的父级目录
dir=os.path.dirname(__file__)
print(dir)
# 获取项目目录路径
BASIDIR=os.path.dirname(dir)
print(BASIDIR)
# 路径拼接
file_path=os.path.join(BASIDIR,r'python\test02.py')
print(file_path)



