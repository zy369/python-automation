# -*- coding: utf-8 -*-
# @Time    : 2021/6/3 13:40
# @Author  : daodao
# @FileName: work12.py
# @Software: PyCharm
# @qq ：402583874
'''
1、上课的手机类继承代码自己敲一遍进行提交，

2、有一组数据，如下格式：
[
{'case_id': 1, 'method': 'post', 'url': '/member/login', 'data': '123', 'actual': '不通过','excepted': '通过'},
{'case_id': 2,  'method': 'post', 'url': '/member/login', 'data': '123','actual': '通过', 'excepted': '通过'},
{'case_id': 3, 'method': 'post', 'url': '/member/login', 'data': '123', 'actual': '不通过','excepted': '通过'},
{'case_id': 4,  'method': 'post', 'url': '/member/login', 'data': '123','actual': '通过', 'excepted': '通过'},
{'case_id': 4, 'method': 'post', 'url': '/member/login', 'data': '123', 'actual': '不通过','excepted': '通过'},
{'case_id': 5,  'method': 'post', 'url': '/member/login', 'data': '123','actual': '通过', 'excepted': '通过'},
]

定义一个如下的类
请列表中的每个字典遍历出来，每个字典的数据用一个对象来保存，
要求：通过setattr 把字典中数据设为对象的属性和值，字典中的key对应属性名，value为属性值。
最后把所有的对象，放入一个列表中，得到如下如格式的数据：
[用例对象，用例对象，用例对象...]
class CaseData:
    pass
3、python基础到此就差不多结束了，本次不布置过多的作业，将基础阶段的基本语法，内容先消化一下，后面就是类的各种应用了
'''
#----------------第一题------------------------
#  需求一，大哥大手机（1995）
class BasePhone:

    def call_phone(self):
        print("这个是拨打语言电话的功能")


# 需求二：功能机（2005年左右
class PhoneV1(BasePhone):

    def display_music(self):
        print("播放音乐")

    def send_msg(self):
        print("发送信息")


# 需求三：智能机（2019）
class PhoneV2(PhoneV1):

    # 重写父类方法
    def call_phone(self):
        print("拨打视频电话")
        print("视频电话5分钟后，切换语言通话")
        # 调用父类的同名方法，
        # BasePhone.call_phone(self)
        super().call_phone()


    def game(self):
        print("打游戏")


phone = PhoneV2()
phone.call_phone()
# phone.display_music()
# phone.game()
#----------------第二题------------------------

class CaseData:
    pass

datas=[
{'case_id': 1, 'method': 'post', 'url': '/member/login', 'data': '123', 'actual': '不通过','excepted': '通过'},
{'case_id': 2,  'method': 'post', 'url': '/member/login', 'data': '123','actual': '通过', 'excepted': '通过'},
{'case_id': 3, 'method': 'post', 'url': '/member/login', 'data': '123', 'actual': '不通过','excepted': '通过'},
{'case_id': 4,  'method': 'post', 'url': '/member/login', 'data': '123','actual': '通过', 'excepted': '通过'},
{'case_id': 4, 'method': 'post', 'url': '/member/login', 'data': '123', 'actual': '不通过','excepted': '通过'},
{'case_id': 5,  'method': 'post', 'url': '/member/login', 'data': '123','actual': '通过', 'excepted': '通过'},
]

li=[]
for i in datas:
    case = CaseData()
    for k,v in i.items():
        setattr(case,k,v)
    li.append(case.__dict__)
print(li)

