# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 11:09
# @Author  : daodao
# @FileName: 02字典的使用.py
# @Software: PyCharm
# @qq ：402583874
'''
list  []
tuple  ()
dict   {}
字典的定义：通过{} 来表示字典
字典中的每一条数据是由key:value 组成，每对键值用逗号隔开
字典中的key是唯一的
key 只能是不可变的数据类型
value 可以是任意数据类型
不可变数据类型：数值、字符串、tuple

字典是可变数据类型支持增删改查的操作
'''
#字典的定义
dict1={'name':'小明','age':18,'height':180.00}
print(dict1,type(dict1))
# 空字典的定义
dict2={}
print(dict2,type(dict2))

#字典的增加和修改方法
dic3 = {"aa": 11, "bb": "22", "cc": 22}
dic3['dd']=99# 通过键可以增加元素
print(dic3)
dic3['aa']=999
print(dic3)# 通过键可以修改元素
dic3.update({'a':1,'b':2})# update 一次增加多个元素
print(dic3)

# 字典的删除方法
dic3.pop('aa')# pop() 通过指定键删除元素
print(dic3)
dic3.popitem()# popitem 删除字典中最后一个元素
print(dic3)
dic3.clear()# clear() 清空字典
print(dic3)

# dic4 = {"aa": 11, "bb": "22", "cc": 22}
# del dic4['aa']  # del 删除数据的关键字 可以删除任何数据
# print(dic4)
# del dic4
# print(dic4)

# 字典的查询方法
dic = {"aa": 11, "bb": "22", "cc": 22}
print(dic['aa']) # 通过键查询值
print(dic.get('aa'))# get 方法，通过键查询对应的值
print(dic.keys(),type(dic.keys())) # keys() 查询字典中所有的键
print(dic.values(),type(dic.values())) # values() 查询字典中所有的值
print(dic.items(),type(dic.items())) # items() 查询字典中所有的键值对
print(dic) # print 打印输出字典

# 字典第一的方式扩展
## 第一种 {} 定义字典
dic5 = {'aa': 999, 'bb': '22', 'cc': 22, 'dd': 999, 'ff': 999, 'a': 1, 'b': 1, 'c': 3}
## 第二种 dict()
dict6=dict(name="小明", age=18, gender="男")
print(dict6)

## 第三中 dict
#[("aa", 11), ("bb", 22), ("cc", 33)] 列表嵌套元组
dict7=dict([("aa", 11), ("bb", 22), ("cc", 33)])
print(dict7)
