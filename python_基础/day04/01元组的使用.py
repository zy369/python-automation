# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 10:33
# @Author  : daodao
# @FileName: 01元组的使用.py
# @Software: PyCharm
# @qq ：402583874
'''
列表：[]
元组 ：
    定义通过（） 来定义元组
    元组的数据可以是任意的
    元组是不可变的数据类型
    元组定义后：不能修改元素
元组的方法：
    元组只有两个查找的方法  count 查找元素在元组中出现的次数  index 方法 查找元素在元组中的下标
注意点
空元组怎么定义
    tu=()
元组中只有一个元素怎么定义
    tu=(11,)
'''
# tu=(11,22,33)
# tu[0]=44  #TypeError: 'tuple' object does not support item assignment
# print(tu)
# tu2=(11,22,33)
# res=tu2.count(11)
# print(res)
# res=tu2.index(11)# 查找元素在元组中的下标
# print(res)
# res=tu2.index(333)#ValueError: tuple.index(x): x not in tuple
# print(res)
#空元组的定义
# tu=()
# print(tu,type(tu))
# # 只有一个元素的元组定义
# tu=(11,)
# print(tu,type(tu))
