# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 12:49
# @Author  : daodao
# @FileName: 03集合的定义.py
# @Software: PyCharm
# @qq ：402583874
'''
set 的定义：通过{} 来表示
空集合的定义
    s=set()
set 是可变的数据类型
特性：
    1、元素不可以重复
    2、存放的元素是不可变数据类型
使用场景
    1、对列表去重
    2、区分可变和不可变数据类型
不可变数据类型： 数值、字符、元组
可变数据类型：列表、字典、集合
按照数据结构分类：
1、数值类型 ：整数、浮点数、布尔值
2、序列类型：字符串、列表、元组（可以下标取值，支持切片）
3、散列类型： 字典 集合

使用频率
列表 > 字典 >元组

'''
set1={11,22,33}
print(set1,type(set1))
set2=set() # 定义空集合
print(set2,type(set2))

# 去重
set3 = {1, 1, 1, 1, 12, 2, 2, 2, 2, 2, 3, 3, 3}
print(set3)
# 列表快速去重
li = [11, 22, 11, 22, 33, 11, 22, 33, 1, 2, 3]
print(list(set(li)))
#区分可变不可变类型
# li=[1,2,3]
# set4={li}
# print(set4) #TypeError: unhashable type: 'list'
# dic={'a':1}
# set5={dic}
# print(set5)#TypeError: unhashable type: 'dict'
tu=(1,2,3)
set6={tu}
print(set6)