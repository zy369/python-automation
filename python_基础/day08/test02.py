# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 17:05
# @Author  : daodao
# @FileName: test02.py
# @Software: PyCharm
# @qq ：402583874

# 第一种 import 模块名
# import test01
# print(test01.a)
# 第二种 import 模块名 as 别名
# import test01 as t
# t.func1()
# 第三种 from 模块名 import 类名/函数名/变量名
# from test01 import a
# print(a)

# 包导入
# 第一种 from 包名 import 模块名
# from  pack01 import pack01_test1
# print(pack01_test1.name)

# 第二中 from 包名.模块名 import 类名/函数名/变量名

# from pack01.pack01_test1 import name
# print(name)

# 第三种 from 包名.包名 import 模块名
from pack02.pack_test import pack02_test
print(pack02_test.name)