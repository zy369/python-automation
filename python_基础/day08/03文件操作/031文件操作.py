# -*- coding: utf-8 -*-
# @Time    : 2021/5/31 14:21
# @Author  : daodao
# @FileName: 031文件操作.py
# @Software: PyCharm
# @qq ：402583874

'''
文件操作
打开文件  open(参数1，参数2，参数3)
参数1：指定文件
参数2：打开的模式
    r: 读取模式，如果打开的文件不存在，直接报错
    a: 追加写入（在文件中原有的内容最后追加写入），被打开的文件不存在，会自动创建一个
    w: 覆盖写入（清空文件中原有的内容），被打开的文件不存在，会自动创建一个

    # 操作一下图片，视频等文件
    rb: 读取模式，如果打开的文件不存在，直接报错（以二进制模式去打开文件）
    ab: 追加写入（在文件中原有的内容最后追加写入），被打开的文件不存在，会自动创建一个（以二进制模式去打开文件）
    wb: 覆盖写入（清空文件中原有内容），被打开的文件不存在，会自动创建一个（以二进制模式去打开文件）
参数3：编码方式（"utf-8"）
'''
# 打开文件，返回一个操作的句柄
# f=open(file='text.txt',mode='r',encoding='utf-8')
# print(f)

#读取内容

# 第一种读取全部内容
# context=f.read()
# print(context)

# 第二种读取一行内容
# data=f.readline()
# print(data)

# 第三中把所有内容按行读取出来放在列表中
# data2=f.readlines()
# print(data2)

# 文件写入
# f=open('text.txt','a',encoding='utf-8')
# f.write('python')

# 以二进制的模式去打开文件
# f=open('微信图片_20210514104050.png','rb')
# print(f.read())
# 关闭文件
# f.close()

# with: 开启open返回文件句柄对象的上下文管理器（执行完with的代码语句后，会自动关闭文件）
with open('text.txt','r',encoding='utf-8') as f:
    c=f.read()
    print(c)

