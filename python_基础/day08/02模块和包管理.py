# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 16:58
# @Author  : daodao
# @FileName: 02模块和包管理.py
# @Software: PyCharm
# @qq ：402583874
'''
模块和包管理
模块和包导入的路径：和当前文件在同级目录下可以直接导入

模块导入方式
1、import 模块名
2、import 模块名 as 别名
3、from 模块名 import 类名/函数名/变量名
包导入方式
1、 from 包名 import 模块
2、 from 包名.模块名 import 类名/函数名/变量名
3、 from 包名.包名 import 模块名

推荐使用
import 模块名
from 包名 import 模块名
from 包名.包名 import 模块名

包和文件夹的区别
1、包里多了一个__init__.py文件
2、导入包时，包里__init_.py文件会自动执行。
'''