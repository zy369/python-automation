# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 16:43
# @Author  : daodao
# @FileName: 01高级内置函数.py
# @Software: PyCharm
# @qq ：402583874
'''
enumerate 获取 字符串/列表/元组每个元素和对应的下表
filter 过滤器函数
匿名函数
'''
#enumerate 获取 字符串/列表/元组每个元素和对应的下表
s = "sfsdgfsk"
li2=enumerate(s)
print(list(li2))

# filter 过滤器函数
# 参数1：过滤的规则函数
# 参数2：要被过滤的数据

# 将case_id大于3的用例数据过滤出来
res1 = [
    {'case_id': 1, 'case_title': '用例1', 'url': 'www.baudi.com', 'data': '001', 'excepted': 'ok'},
    {'case_id': 4, 'case_title': '用例4', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 2, 'case_title': '用例2', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 3, 'case_title': '用例3', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 5, 'case_title': '用例5', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'}
]

def func(data):
    return data['case_id']>3
# 使用过滤器对数据进行过滤
res= list(filter(func,res1))
print(res)

# 匿名函数
f=lambda x,y:x+y
print(f(1,3))
