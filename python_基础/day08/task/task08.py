# -*- coding: utf-8 -*-
# @Time    : 2021/5/31 14:48
# @Author  : daodao
# @FileName: task08.py
# @Software: PyCharm
# @qq ：402583874
'''
文件操作
第一题：当前有一个txt文件，内容如下：
数据aaa
数据bbb
数据ccc
数据ddd
# 要求：请将数据读取出来，转换为以下格式
{'data0': '数据aaa', 'data1': '数据bbb', 'data2': '数据ccc', 'data3': '数据ddd'}
​
# 提示：
# 可能会用到内置函数enumerate
​
# 注意点：读取出来的数据如果有换行符'\n'，要想办法去掉。
第二题：当前有一个case.txt文件，里面中存储了很多用例数据: 如下，每一行数据就是一条用例数据，



# 文件中数据
url:www.baidu.com,mobilephone:13760246701,pwd:123456
url:www.baidu.com,mobilephone:15678934551,pwd:234555
url:www.baidu.com,mobilephone:15678934551,pwd:234555
url:www.baidu.com,mobilephone:15678934551,pwd:234555
url:www.baidu.com,mobilephone:15678934551,pwd:234555

# 要求一： 请把这些数据读取出来，到并且存到list中，格式如下
[
{'url': 'www.baidu.com', 'mobilephone': '13760246701', 'pwd': '123456'}, {'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},
{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'}
]
# 要求二：将上述数据再次进行转换，转换为下面这种字典格式格式
​
{
   'data1':{'url': 'www.baidu.com', 'mobilephone': '13760246701', 'pwd': '123456'},
   'data2':{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},
   'data3':{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},
   'data4':{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'},
   'data5':{'url': 'www.baidu.com', 'mobilephone': '15678934551', 'pwd': '234555'}
}
​
# 提示：需要使用字符串的分割方法
​
# 注意点：数据中如果有换行符'\n'，要想办法去掉。


第三题：之前作业写了一个注册的功能，再之前的功能上进行升级，要求：把所有注册成功的用户数据放到文件中进行保存，数据存储的格式不限
提示：
每次运行程序，先去文件中读取所有注册过的用户数据，
程序运行完之后，将所有的用户数据再次写入到文件中
可以直接在文件中写个列表
'''
#--------------------第一题-------------------------
def work1():
    # 第一步读取数据，每一行作为一个元素放到列表中
    with open('1.txt','r',encoding='utf-8') as f :
       datas=f.readlines()
    dic={}
    # 第二步将数据转化成字典
    for index,data in enumerate(datas):
        key='data{}'.format(index)
        value=data.replace('\n','')
        dic[key]=value
    return dic
#--------------------第二题-------------------------
def work2_1():
    #读取数据
    with open('case.txt','r',encoding='utf-8') as f:
        datas=f.readlines()
    cases=[]
    # 数据转化成字典，放到列表中
    for i in datas:
        item=i.split(',')
        dic={}
        for j in item:
            key=j.split(":")[0]
            value=j.split(':')[1].replace('\n','')
            dic[key]=value
        cases.append(dic)
    return cases

def work2_2(datas):
    dic={}
    for index,data in enumerate(datas):
        key='data{}'.format(index+1)
        dic[key]=data
    return dic

#--------------------第三题-------------------------
def work3():
    with open('users.txt','r',encoding='utf-8') as f:
        users=eval(f.read())
    # 注册功能代码（上次作业写的，不需要改动）
        username = input('请输入新账号:')  # 输入账号
        password1 = input('请输入密码：')  # 输入密码
        password2 = input('请再次确认密码：')  # 再次确认密码

        for user in users:  # 遍历出所有账号，判断账号是否存在
            if username == user['name']:
                print('该账户已存在')  # 账号存在，
                break
        else:
            # 判断两次密码是否一致
            if password1 != password2:
                print('注册失败，两次输入的密码不一致')
            else:
                # 账号不存在 密码一样，则添加到账户列表中
                users.append({'name': username, 'pwd': password2})
                print('注册成功！')

        # 程序运行结束后，将所有用户的数据写入文件
        with open('users.txt', 'w', encoding='utf8') as f:
            # 将列表转换为字符串,写入文件
            f.write(str(users))

if __name__ == '__main__':
    #datas=work2_1()
    # res=work2_2(datas)
    # print(res)
    work3()