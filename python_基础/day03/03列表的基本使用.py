# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 17:27
# @Author  : daodao
# @FileName: 03列表的基本使用.py
# @Software: PyCharm
# @qq ：402583874

'''
列表：是一种可变的数据类型
列表的定义[]
'''
#列表可以通过下标取值
li = [19, 1.3, "python"]
print(li[2])
# 列表的切片操作
li1 = [11, 22, 33, 44, 55]
print(li1[::2])

# 列表中的增删改查的方法
li2=[1,2,3]
## 增加元素的方法 append   insert   exend
# append 向列表尾处添加一个元素（追加）
li2.append('11')
print(li2)
# insert 通过下标指定位置，向列表中加入数据
li2.insert(1,999)
print(li2)
# exend 一次性加入多个元素（放在列表末尾）
li2.extend(['a','b','c'])
print(li2)

# 删除列表中元素的方法  remove   pop  clear
# remove  删除指定的元素
li3 = [1, 2, 3, 11, 22, 33, 1, 2, 3]
li3.remove(1)
print(li3)
#pop  指定下标位置删除元素（默认删除最后一个元素）
li3.pop(3)
print(li3)
# clear 清空列表
li3.clear()
print(li3)

# 查找的方法 index
# index: 查找指定元素的下标，找到后返回下标值，没找到报错
li4=[1, 2, 3, 11, 22, 33, 1, 2, 3]
res=li4.index(11)
print(res)
# count: 统计列表中某个元素出现的次数
res=li4.count(1)
print(res)
# 修改表中某个元素的值，通过下标去指定元素，进行重新赋值
li5=["aa", "bb", 11, 22, 33]
li5[1]=999
print(li5)

# 其他方法
# sort  排序
li=[111, 22, 31, 41, 5, 6, 888, 8, 34, 8, 12, 7, 33]
li.sort() # 对列表进行升序排序
print(li)
li.sort(reverse=True)# 通过参数reverse=True 可以从大到小，降序排列
print(li)
# reverse 方法 将列表头元素变成尾元素，列表尾元素变成头元素
li.reverse()# 将列表反转（头变成尾，尾变成头）
print(li)
# copy 方法 复制 （将原有的列表在内存复制一遍）
li6=li.copy()
li7=li.copy()
print(id(li))
print(id(li6))
print(id(li7))