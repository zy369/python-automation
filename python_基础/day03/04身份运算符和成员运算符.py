# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 9:05
# @Author  : daodao
# @FileName: 04身份运算符和成员运算符.py
# @Software: PyCharm
# @qq ：402583874

'''
成员运算符 in  , not in
'''
li = ["aa", "bb", 11, 22, 33]
print('aa'in li )
print('aa' not in li)
'''
身份运算符 is ,  is not
id: 查看数据的内存地址
'''
a=1000
b=1000
c=1000
print(a is b)
print(a is not b)
print(id(a))
print(id(b))
print(id(c))
li=[11,22,33]
li2=[11,22,33]
print(id(li))
print(id(li2))