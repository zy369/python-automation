# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 16:37
# @Author  : daodao
# @FileName: 02字符串的格式化输出.py
# @Software: PyCharm
# @qq ：402583874

'''
字符串的格式化输出
1、传统的格式化方法：%
2、format格式化输出
3、F表达式

'''
# 有几个{}，format 就可以传入几个数据，按照位置一一对应填入
str1 = "今收到{}，交了学费{}元，开此收据为凭证"
print(str1.format('小明',500))
# 通过下标指定位置填入数据
str1 = "今收到{0}，交了学费{0}元，开此收据为凭证".format('小明',500)
print(str1)
# 保留小数点后面的位数
print('今天XX股票的价格是：{:.4f}'.format(7))
# 将小数按照百分比的形式显示
print('百分比为：{:.2%}'.format(0.12))

# 格式化字符串长度
print('{:<8}******'.format('abc'))#左对齐8个字符不够补空格
print('{:9<8}******'.format('abc'))
print('{:>8}******'.format('abc'))#右对齐8个字符不够补空格
print('{:9>8}******'.format('abc'))
print('{:^8}******'.format('abc'))#中间对齐8个字符不够补空格
print('{:y^8}******'.format('abc'))

# 传统的格式化输入法：%
# %s 为字符占位（任意类型都可以）
# %d 为数值类型占位
# %f  为浮点类型占位
s='abd123--%s--%s--%s'%('A','B','C')
print(s)
s='我的名字%s,体重%dkg,性别%s'%('小明',80,'男')
print(s)
# F表达式格式化输出
name='小明'
age=18
s=F'我的名字{name}，年龄{18}'
print(s)
