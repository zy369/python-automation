# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 16:17
# @Author  : daodao
# @FileName: 01字符串的相关方法.py
# @Software: PyCharm
# @qq ：402583874

'''
字符串的方法：find  count  replace split upper lower
方法的调用：字符串.方法名（）

'''
# find
s1 = "abcd234abcabcd234abc"
# 查找子串在主串中的位置，从前往后找，找到匹配的就返回子串的位置（子串第一个字母在主串中的位置）
# find(a,b) 第一个参数是子串，第二个参数是查找的起点
res=s1.find('s',4)
print(res)

# count 计算子串在主串中出现的次数
res=s1.count('abc')
print(res)

# replace 替换指定的子串
# 参数1：新的子串
# 参数2：旧的子串
# 参数3：替换的此数 (默认是全部)
res=s1.replace('abc','ABC',1)
print(res)

# split 指定分割点对字符串进行fenge
#参数1：分割点
#参数2：分割次数（默认是全部）
res=s1.split('a',1)
print(res)

# upper 将小写字符转换成大写字母
s='abc123CDEF'
res=s.upper()
print(res)

#lower 将大写字符转换成小写
res=s.lower()
print(res)