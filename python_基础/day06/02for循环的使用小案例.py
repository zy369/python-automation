# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 10:57
# @Author  : daodao
# @FileName: 02for循环的使用小案例.py
# @Software: PyCharm
# @qq ：402583874

# users = [{"name": "py01", "pwd": "123"},
#          {"name": "py03", "pwd": "123"},
#          {"name": "py04", "pwd": "123"}]
# # 需求查找用户 py03 是否在列表中
#
# for i in users:
#     if i['name']=='py03':
#         print(f'找到了这个用户{i}')
#         # continue
#         break
#     print(f'对比完用户{i}')
# else:
#     print('没有找到这个用户')

# for 循环的嵌套使用
'''
打印如下图形
1
12
123
1234
12345

*
* *
* * *
* * * *
'''
for i in range(1,6):
    for j in range(1,i+1): # 内层循环，根据行数打印数据 第一行打印1，第二行打印1 2 第三行打印 1 2 3...
        print(j,end='')
    print('')
for i in range(1,5):
    for j in range(1,i+1):
        print("* ",end='')
    print('')