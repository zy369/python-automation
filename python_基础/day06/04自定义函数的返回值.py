# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 11:12
# @Author  : daodao
# @FileName: 04自定义函数的返回值.py
# @Software: PyCharm
# @qq ：402583874
'''
函数的返回值：
    函数的返回值是有return 决定的
    函数中没有return 函数的返回值默认None
    return 后面没有内容，返回值也是None
    函数要返回多个值 return 后面每个数据用逗号隔开
return 作用
    1、返回数据
    2、终止函数，跳出函数
'''
def func():
    print('python')
    # return [666,777]
    return 'python'
res=func()
print(res)