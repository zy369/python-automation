# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 10:58
# @Author  : daodao
# @FileName: 03函数的定义和使用.py
# @Software: PyCharm
# @qq ：402583874

'''
函数用法：
    函数名（）
函数的作用：对特定的一些功能进行封装，提高代码的重用率，进而提升开发的效率
函数的定义：
def 函数名（）：
    #函数体
函数名的命名规则：
    字母、下划线和数字组成，且不能用数字开头，不能使用python中的关键字
函数命名的风格：
    1、单词之间用下滑线隔开（python 中函数命名更推荐这种风格）
    2、大驼峰
    2、小驼峰
函数执行后，有没有数据是根据函数的返回值来决定的
'''
# 定义一个打印三角形的函数

def func():
    for i in range(1,6):
        for j in range(1,i+1):
            print(f'{j}',end='')
        print('')
func()