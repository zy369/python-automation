# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 11:38
# @Author  : daodao
# @FileName: task06.py
# @Software: PyCharm
# @qq ：402583874

'''

作业（每一道题封装成一个函数）
1、一、输出99乘法表，结果如下：（提示嵌套for循环，格式化输出）
2、有1 2 3 4 这四个数字，设计程序计算能组成多少个互不相同且无重复数字的3位数？分别是什么？
3、通过函数实现一个计算器，运行程序分别提示用户输入数字1，数字2，然后再提示用户选择 ：加【1】减【2】乘【3】除【4】，
根据不同的选择完成不同的计算
每个方法使用一个函数来实现， 选择后调用对应的函数，然后返回结果。
4、学习控制流程时，我们讲了一个登录的案例，现在要求大家通过代码实现一个注册的流程，
基本要求：
1、运行程序，提示用户，输入用户名，输入密码，再次确认密码。
2、判读用户名有没有被注册过，如果用户名被注册过了，那么打印结果该用户名已经被注册。
3、用户名没有被注册过，则判断两次输入的密码是否一致，一致的话则注册成功，否则给出对应的提示。
'''
# print('------------第一题-----------------')
#
# def mu_table():
#     for i in range(1,10):
#         for j in range(1,i+1):
#             print(f'{i} * {j} = {i*j:<4}',end='')
#         print()
# mu_table()
# print('------------第二题-----------------')
# def count_num():
#     count=0
#     for a in range(1,5):
#         for b in range(1,5):
#             for c in range(1,5):
#                 if a!=b and a!=c and b!=c :
#                     print(a,b,c)
#                     count+=1
#     print(f'一共{count}个')
# count_num()
# print('------------第三题-----------------')
#
# def add_num (a,b):
#     return a+b
# def sub_nub(a,b):
#     return a-b
# def mul_num(a,b):
#     return a*b
# def div_num(a,b):
#     return a/b
#
# def computer_count():
#     a=int(input('输入数字:'))
#     b = int(input('输入数字:'))
#     num=int(input('请输入选项数字：加【1】减【2】乘【3】除【4】'))
#     if num == 1:
#         return add_num(a,b)
#     elif num==2:
#         return sub_nub(a,b)
#     elif num==3:
#         return mul_num(a,b)
#     elif num==4:
#         return div_num(a,b)
#     else:
#         print('没有此选项')
# res=compute_number()
# print(res)
print('------------第四题-----------------')

users = [{"name": "py01", "pwd": "123"},
         {"name": "py02", "pwd": "123"},
         {"name": "py03", "pwd": "123"},
         {"name": "py04", "pwd": "123"}]
def register():
    username=input("请输入注册账号：")
    password=input("请输入密码：")
    password2=input("请再次输入密码：")
    for user in users:
        if username==user['name']:
            print('该用户名已经被注册')
            break
    else:
        if password != password2:
            print('两次输入的密码不一致')
        else:
            users.append({'name':username,'pwd':password})
            print('注册成功')
register()
print(users)

