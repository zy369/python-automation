# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 11:22
# @Author  : daodao
# @FileName: 05 函数的参数.py
# @Software: PyCharm
# @qq ：402583874
'''
函数的参数：
    形参和实参
    定义的参数为形参
    调用实际传达的为实参
参数传递：
    调用函数的时候，参数传递的方式
    位置传参：通过位置传递参数（按照顺序传递，第一个实参传给第一个形参）
    关键字传参：通过参数名指定传给某个参数（传参的时间不注意参数的位置）
参数定义的方式
    1、必备参数(必须参数)：直接写个参数名
    2、缺省参数（默认参数）：定义的时候给参数设置一个默认值
    3、可变参数（动态参数，不定长参数）：*arg,**kwargs

'''
def add_number(a,b,c=10,*args,**kwargs):
    return a+b+c
res=add_number(1,2)#位置传参
print(res)

res2=add_number(a=3,b=4)# 关键字传参
print(res2)