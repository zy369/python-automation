# -*- coding: utf-8 -*-
# @Time    : 2021/6/1 17:05
# @Author  : daodao
# @FileName: 01异常捕获处理.py
# @Software: PyCharm
# @qq ：402583874

'''
except 可以指定捕获异常类
assert 断言  比较两个数据是否一致
raise 抛出异常，打印到控制台
'''
# 捕获单个异常类型
# try:
#     print(a)
# except NameError:
#     print('捕获到了异常')

# 捕获多个异常类型(不同的异常类型需要做不同的处理时候)

# try:
#     print(a)
#     int('a')
# except NameError as e:
#     print(f'捕获异常{e}')
# except ValueError as e2:
#     print(f'捕获异常{e2}')

# 捕获多个异常类型（不同的异常类型，做统一处理的时候）
# try:
#     print(a)
#     int('a')
# except (NameError,ValueError) as e:
#     print(f'捕获异常{e}')

# 捕获所有异常类型

# try:
#     int('a')
#     print(a)
# except:
#     print('捕获异常')

# 小案例

result='8888'
expected='888'

try:
    assert  result ==expected
except AssertionError as e:
    print('用例未通过')
    raise e
