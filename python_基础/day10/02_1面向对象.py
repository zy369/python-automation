# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 11:23
# @Author  : daodao
# @FileName: 02_1面向对象.py
# @Software: PyCharm
# @qq ：402583874
'''
面向对象
int(类)：1，2，3...
str（类）：’ab','a'
list(类）：[1,3]

类（类型）和对象（这个类型中的一个具体的数据）
类的定义：
class 类名():
    pass
类名的规范：大驼峰的形式（每一个单词第一个字母大写）

通过类去创建对象
类名()
'''

class GirlFriend():
    pass

obj1=GirlFriend()
print(obj1)
