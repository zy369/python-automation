# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 13:51
# @Author  : daodao
# @FileName: 03self和方法.py
# @Software: PyCharm
# @qq ：402583874

'''
方法：定义在类中的函数
方法的调用：对象名.方法名()
self: 代表的是对象本身，哪个对象去调用方法，那么self就代表的是那个对象

'''
class GirlFriend():
    gender='女'
    def __init__(self,name,face,height,leg):
        self.name=name
        self.face=face
        self.height=height
        self.leg=leg

    def skill(self):
        print(f'{self.name}会逛街')

obj1=GirlFriend('小花','好看',170,'一米')
obj2=GirlFriend('小红','好看',170,'一米')
obj1.skill()
obj2.skill()
