# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 14:05
# @Author  : daodao
# @FileName: 04类方法和实例方法.py
# @Software: PyCharm
# @qq ：402583874

'''
对象方法（实例方法）：
实例方法的定义：直接定义在类中的函数  第一个参数是self,self代表的是对象本身
实例方法的调用： 对象名.方法（）

类方法：
类方法定义：要通过classmethod装饰器来声明一个类方法
第一个参数是cls,cls代表的是类本身
类方法的调用：类名.方法（） 或 对象名.方法（）

'''
class GirlFriend():
    gender='女'
    def __init__(self,name,face,height,leg):
        self.name=name
        self.face=face
        self.height=height
        self.leg=leg

    def skill(self):
        print(f'{self.name}会逛街')

    @classmethod
    def skill2(self):
        print('看电视')

obj1=GirlFriend('小花','好看',170,'一米')

#  类方法的调用
obj1.skill2() # 实例对象.方法()
GirlFriend.skill2() # 类名.方法（）
# 实例对象的调用
obj1.skill()
# GirlFriend.skill() #TypeError: skill() missing 1 required positional argument: 'self'  类不能调用实例对象的方法