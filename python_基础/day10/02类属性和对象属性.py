# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 10:29
# @Author  : daodao
# @FileName: 02类属性和对象属性.py
# @Software: PyCharm
# @qq ：402583874

'''
类里面应该有该类事务共同的特征和行为
特征：属性
行为：方法

类属性：这个类所有的对象都有这个属性，属性值是一样的
类属性的定义：直接定义在类里面的变量，类属性
类属性值的获取：可以通过对象来获取，也可以用来来获取
获取类属性值： 类名.属性名 对象名.属性名

对象属性（实现属性）：这个类所有的对象都可以有这个属性，每个对象的属性值可以不一样
实例属性的定义：对象名.属性名=属性值
实例属性值的获取：对象名.属性名

方法：定义在类里的函数
__init__ 方法 初始化方法（初始化对象的属性） 创建对象的同时添加对象属性
'''
class GirlFriend:
    '''
    女朋友类
    '''
    gender='女'
    def __init__(self,name,face,hieght,leg):
        self.name=name
        self.face=face
        self.height=hieght
        self.leg=leg
obj1=GirlFriend('球总监','球脸',176.5,'两米')
obj2=GirlFriend('小amy','amy脸',160,'一米')
obj3=GirlFriend('好人','好人脸',176,'一米')
obj4=GirlFriend('拆拆','拆拆脸',176,'一米')
obj5=GirlFriend('华健','贱脸',190,'一米')
# print(obj1.__dict__)
# print(obj2.__dict__)
# print(obj3.__dict__)
# print(obj4.__dict__)
# print(obj5.__dict__)

# 获取属性值
## 类属性值的获取:可以通过对象来获取，也可以通过类获取
# print(obj1.gender)
# print(GirlFriend.gender)
## 对象（实例）属性值的获取：只能通过对象来获取
print(obj1.name)
