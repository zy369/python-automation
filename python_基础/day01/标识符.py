# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 13:48
# @Author  : daodao
# @FileName: 标识符.py
# @Software: PyCharm
# @qq ：402583874

'''
标识符：只要自己起的名字都是标识符
    变量、函数名、类名、模块名（py文件)、包名
标识符的命名规范：只可以用字母、数字、下划线组成（不能用数字开头）
变量的命名要见名知意（尽量不要使用拼音）

标识符命名时不能有python关键字
python关键字：每个关键字都是有特殊作用的  36个关键字
['False', 'None', 'True', '__peg_parser__', 'and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue',
'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda',
'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']



'''
import keyword
print(keyword.kwlist)
print(len(keyword.kwlist))