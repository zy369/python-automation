# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 13:56
# @Author  : daodao
# @FileName: print 和input.py
# @Software: PyCharm
# @qq ：402583874
'''
print:输出
input:输入
'''
name=input('请输入你的名字：')
age=input('请输入你的年龄：')
gender=input('请输入你的性别：')
print(name,age,gender)