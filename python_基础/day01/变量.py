# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 13:38
# @Author  : daodao
# @FileName: 变量.py
# @Software: PyCharm
# @qq ：402583874
'''
变量
变量的命名规范：只可以用字母、数字、下划线组成（不能用数字开头）
变量的命名要见名知意（尽量不要使用拼音）

常见的命名方法（风格）：
    1、纯小写字母，每个单词之间用下划线隔开
    max_num=999
    2、大驼峰命名法：每个单词的第一个字母大写
    Max_Num=999
    3、小驼峰命名法：第一个单词首字母小写，后面单词首字母大写
    max_Num=999
'''
name='小明'
age=18
gender='男'
print(name,age,gender)
#不要使用拼音命名
xiaoming='小明'