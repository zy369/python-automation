# -*- coding: utf-8 -*-
# @Time    : 2021/5/24 14:03
# @Author  : daodao
# @FileName: task01.py
# @Software: PyCharm
# @qq ：402583874
'''
"""
1、下面那些不能作为标识符？
1、find     2、 _num    3、7val        4、add.       5、def
6、pan      7、-print   8、open_file   9、FileName   10、9print
11、INPUT   12、ls      13、user^name  14、list1     15、str_
16、_888    17、is      18、true       19、none      20、try

答案：
    7val     add.       def    -print
    9print   user^name  is     try



2、写一段代码，运行的时候依次提示输入姓名，年龄、性别
然后在控制台 按照以下格式输出信息  ：姓名、年龄、性别 ：

答案：

name = input("请输入姓名:")
age = input("请输入年龄:")
gender = input("请输入性别:")

print("*********************")
print("姓名:", name)
print("年龄:", age)
print("性别:", gender)
print("*********************")


3、请描述一下变量的命名规范，和有哪几种命名风格？（简单题）

答案：
    变量的命名规范：只可以使用数字、字母和下划线组成(不能使用数字开头),不能够使用python中的关键字
    变量命名尽量做到见名知意（尽量不要使用拼音）

    常见的命令命名表示法（风格）：
    1、纯小写字母,每个单词之间用下划线隔开(下划线)
    2、每个单词的第一个字母大写(大驼峰)
    3、第一单词开头字母小写，后面单词第一个字母大写(小驼峰)


4、把今天学的python基本语法，总结成笔记

'''