# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 17:07
# @Author  : daodao
# @FileName: task05.py
# @Software: PyCharm
# @qq ：402583874
"""
1、一家商场在降价促销。如果购买金额50-100元(包含50元和100元)之间，会给打9折，
如果购买金额大于100元会打8折。编写一程序，询问购买价格，再打印出折扣和最终价格。

2、一个 5 位数，判断它个位与万位相同，十位与千位相
同。 根据判断打印出相关信息。

3、利用random函数生成随机整数，从1-9取出来。然后输入一个数字，来猜，如果大于，则
印大于随机数。小了，则打印小于随机数。如果相等，则打印等于随机数


4、使用循环和条件语句完成剪刀石头布游戏，提示用户输入要出的拳 ：
石头（1）／剪刀（2）／布（3）/退出（4）
电脑随机出拳比较胜负，显示 用户胜、负还是平局。运行如下图所示：

# 用户胜利
user       random
1             2     1
2             3     1
3             1     -2

# 平局
用户和电脑数一样的

# 电脑胜利：

if random-user ==1 or random-user==-2:
    print(用户胜利)
elif random==user:
    print(平局)
else:
    print(电脑胜利)

"""
print('----------第一题--------------')
# price=int(input('请输入购买价格：'))
# if price<50:
#     print(f"没有折扣，您需要支付{price}")
# elif 50<=price<=100:
#     print(f"购买金额50-100元(包含50元和100元)之间，会给打9折,你需要支付{price*0.9}")
# else :
#     print(f"购买金额大于100，给打8折，您需要支付{price * 0.8}")

print('----------第二题--------------')
# num_str=input('请输入5位数：')
# if num_str[0]==num_str[-1] and num_str[1]==num_str[-2]:
#     print("相等")
# else:
#     print('不相等')
print('----------第三题--------------')
# import random
# num=random.randint(1,9)
# x=int(input('请输入一个数：'))
# if x >num:
#     print('大于随机数')
# else :
#     print('小于随机数')
print('----------第四题--------------')
import random
print('------------游戏开始-------------------')
li = ['石头', '剪刀', '布']
while True:
    print('石头（1）／剪刀（2）／布（3）/退出（4）')
    u_num=int(input('请输入选项：'))
    r_num=random.randint(1,3)
    if 1<=u_num<=3:
        if r_num-u_num==1 or r_num-u_num==-1:
            print(f'您出拳为{li[u_num-1]},电脑出拳为{li[r_num-1]}您胜利了')
        elif r_num==u_num:
            print(f'您出拳为{li[u_num-1]},电脑出拳为{li[r_num-1]}平局')
        else:
            print(f'您出拳为{li[u_num-1]},电脑出拳为{li[r_num-1]}电脑胜利')
    elif u_num==4:
        print('游戏结束')
        break
    else:
        print('输入选项错误')