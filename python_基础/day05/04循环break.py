# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 17:06
# @Author  : daodao
# @FileName: 04循环break.py
# @Software: PyCharm
# @qq ：402583874
'''
break 和 continue
break:终止循环跳出循环体
continue: 终止当前本轮循环，开启下轮循环

while 中的else
循环条件不成立，退出循环执行else中的代码
使用break 跳出循环不会执行else中的代码
'''
# 计算 1+2+3+....100
n=1
s=0
# while n<=10:
#     s+=n
#     print(f'n=={n},s=={s}')
#     if n==5:
#         break
#     n+=1
# print(f'n=={n},和等于{s}')
# while n<=10:
#     s+=n
#     print(f'n=={n},s=={s}')
#     n += 1
#     if n==5:
#         continue
#
# print(f'n=={n},和等于{s}')

# while n<10:
#     print(n)
#     n += 1
#     if n==5: #循环条件不成立，退出循环执行else中的代码
#         break
#     else:
#         print('打印while循环中的else')
# while n<10:
#     print(n)
#     n += 1
#     if n==5:
#         break #使用break 跳出循环不会执行else中的代码
# else:
#     print('打印while循环中的else')
def func():
    for i in range(10):
        s=yield i
        print(f'send {s}')
g=func()
print(next(g))
res=g.send(5)
print(res)
print(next(g))