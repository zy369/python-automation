# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 16:52
# @Author  : daodao
# @FileName: 05for循环.py
# @Software: PyCharm
# @qq ：402583874
'''
for 循环 遍历循环
range(): 左闭右开
range(n)
range(n,m)
range(n,m,k)
'''
li = ["小明", "小张", "小李"]
for name in li :
    print(name)
li2=list(range(100))
print(li2)
# 遍历列表
for i in li2:
    print(f'第{i}遍hello pthon')
#遍历range
for i in range(100):
    print(f'第{i}遍hello pthon')
# 遍历字符串
s = "abchgd"
for i in s:
    print(i)
dic = {"aa": 11, "bb": 22, "cc": 33}
# 遍历字典
for d in dic:
    print(d)# 直接遍历得到的是键
for k in dic.keys():
    print(k)

for v in dic.values(): # 遍历字典值
    print(v)

for k,v in dic.items():# 遍历字典键值对
    print(k,v)

#元组拆包
tu=(11,22)
a,b=tu
print(a,b)
# 同时定义两个变量
a,b=888,999
print(a,b)