# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 15:06
# @Author  : daodao
# @FileName: 02条件判断登陆案例.py
# @Software: PyCharm
# @qq ：402583874
'''
登陆的小案例
关键字pass:没有任何语义，占个位置，表示通过的意思
'''
user = {"user": "python", "pwd": "123"}
username=input('请输入有户名：')
password=input('请输入密码：')
if username==user['user'] and password==user['pwd']:
    print('登陆成功')
else:
    print('用户名或密码错误')