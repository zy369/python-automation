# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 14:02
# @Author  : daodao
# @FileName: 01条件判断.py
# @Software: PyCharm
# @qq ：402583874
'''
if 语法规则：
    if 条件：
        条件成立时会执行的代码
    else:
        条件不成立时会执行的代码
python 中是通过：（冒号）来区分代码块的

一个条件语句
    可以由单独的if来组成：（条件成立需要做：事情1，条件不成立不需要处理）
    也可以由if----else-----来组成：（条件成立需要做：事情1，不成立需要做事情2）
    也可以由if----elif :有多个条件，不同的条件需要做不同事情
    还可以由 if---elif---...else 来组成（有多个条件，不同的条件需要做不同的事情，所有的条件不成立也要做特殊处理）
'''
score=float(input('请输入成绩：'))
# if score >60:
#     print('考试及格')
# else:
#     print('考试不及格')
#
if score<60:
    print('考试不及格')
# 考试成绩等级区分
'''
40 > x            E
40<= x <60        D 
60<= x <75        C
75<= x <85        B
85<= x <=100      A
'''
if score <40:
    print('E')
elif 40<=score<60:
    print('D')
elif 60<=score<75:
    print('C')
elif 75<=score<85:
    print('B')
elif 85<=score<=100:
    print("A")
else:
    print('输入错误')