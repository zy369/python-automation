# -*- coding: utf-8 -*-
# @Time    : 2021/5/26 15:12
# @Author  : daodao
# @FileName: 03while循环.py
# @Software: PyCharm
# @qq ：402583874
'''
while 条件：
    循环体
'''
a=10
while a>1:
    print(f'循环体{a}')
    a-=1
# 需求：打印100遍hello python,
# 需求：第50遍打印两遍
n=100
while n>=1:
    print(f'第{n}hello python')
    if n==50:
        print(f'第{n}hello python')
    n-=1