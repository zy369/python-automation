# -*- coding: utf-8 -*-
# @Time    : 2021/6/3 9:54
# @Author  : daodao
# @FileName: 05动态属性设置.py
# @Software: PyCharm
# @qq ：402583874

'''
对象的__dict__属性：获取对象的所以属性以字典形式返回
动态属性设置：
内置函数setattr(参数1，参数2，参数3)
参数1：对象
参数2：给对象设置属性名（字符串类型）
参数3:属性值
'''

# class Cases():
#     def __init__(self,case_id,title,url,data):
#         self.case_id=case_id
#         self.title=title
#         self.url=url
#         self.data=data
# data = {'case_id': 1, 'title': '用例1', 'url': 'www.baudi.com', 'data': '001'}
# # 需求字典中的key为属性，value为属性值
# case= Cases(**data)
# print(case.__dict__)

# setattr 动态属性设置
data = {'case_id': 1, 'title': '用例1', 'url': 'www.baudi.com', 'data': '001'}
class MyClass():
    '''定义一个类'''
    pass

m=MyClass()
# setattr(m,'name','daodao')
# print(m)
# print(m.name)
# print(MyClass.__dict__)
# print(m.__dict__)

# for i in range(1,11):
#     setattr(m,f'data{i}',i)
# print(m)
# print(m.__dict__)
# print(MyClass.__dict__)

for k,v in data.items():
    setattr(m,k,v)
print(m)
print(m.__dict__)

# 获取属性值
values=getattr(m,"case_id")
print(values)
#删除属性值
delattr(m,'case_id')

values=getattr(m,"case_id")


