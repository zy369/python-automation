# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 14:53
# @Author  : daodao
# @FileName: 01类方法和静态方法.py
# @Software: PyCharm
# @qq ：402583874
'''
类里面的属性和方法
属性：
    类属性：直接定义在类里的变量，叫做类属性，类属性可以通过类访问，可以通过实例对象访问
          公有属性：不管类里面还是类外面都可以访问
         私有属性：两个下划线开头的属性叫做私有属性，只能在类里面访问，在类外部是无法使用的

    实例属性：
        实例属性的定义：对象.属性名=属性值
        实例属性只能通过对象访问
方法：
    实例方法：
    第一个参数是self,self代表实例对象本身
    只能使用实例对象调用
    实例方法一般是以实例对象为主体去调用的

类方法：
    第一个参数是cls,cls 代表类的本身
    可以实例对象调用，也可以类调用
    类的方法一般以类为主体调用的
静态方法：
    没有必须要定义的参数
    可以类调用，也可实例对象调用
    静态方法调用的时候，内部不会使用到对像和类的相关属性。
'''
import random

class MyClass():

    attr=100
    __attr=200

    @classmethod
    def cls_func(cls):
        print(f'{cls}类方法')

    def func(self):
        phone=self.random_phone()
        print(f'{MyClass.__attr}收机号码{phone}')

    @staticmethod
    def random_phone():
        phone='13'
        for i in range(9):
            phone+=str(random.randint(0,9))
        return phone

# print(MyClass.attr)
# print(MyClass.__attr) #类外面无法访问私有属性
m=MyClass()
m.func()
m.cls_func()
MyClass.cls_func()