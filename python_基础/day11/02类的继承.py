# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 15:20
# @Author  : daodao
# @FileName: 02类的继承.py
# @Software: PyCharm
# @qq ：402583874

'''
python3中默认基类object
'''
class MyTest:
    pass

class MyTest2(object):
    pass

m1=MyTest()
m2=MyTest2()
print(m1.__dict__)
print(m2.__dict__)