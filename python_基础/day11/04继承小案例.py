# -*- coding: utf-8 -*-
# @Time    : 2021/6/3 9:19
# @Author  : daodao
# @FileName: 04继承小案例.py
# @Software: PyCharm
# @qq ：402583874

'''
继承的作用，提高代码的重用率
重写父类方法：在子类定义一个和父类相同的方法，这个操作叫重写父类方法
在子类中调用父类同名方法：
方式一：父类名.方法名（self）
方式二：super().方法名(self)
推荐使用：方式二
'''

# 需求一，大哥大收机(1995)
class BasePhone():

    def call_phone(self):
        print('这是拨打语音电话的功能')

# 需求二，功能机（2005）
class PhoneV1(BasePhone):
    def play_music(self):
        print('播放音乐')

    def send_msg(self):
        print('发送信息')

# 需求三：智能机（2019）
class PhoneV2(PhoneV1):

    # 重写父类方法
    def call_phone(self):
        print('拨打视频电话')
        print('视频通话5分钟后，切换语音通话')
        super().call_phone() #调用父类同名方法

    def game(self):
        print('打游戏')

p=PhoneV2()
p.call_phone()
p.play_music()
p.send_msg()
p.game()