# -*- coding: utf-8 -*-
# @Time    : 2021/6/2 15:58
# @Author  : daodao
# @FileName: 03类的继承.py
# @Software: PyCharm
# @qq ：402583874
'''

'''

class  BassClass(object):
    money=10
    def func(self):
        print('赚钱的方法，一次赚1000万')

class ClassV1(BassClass):
    # 通过继承可以获得父类的方法和属性
    pass

c=ClassV1()
print(c.money)
c.func()