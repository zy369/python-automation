# -*- coding: utf-8 -*-
# @Time    : 2021/6/3 11:10
# @Author  : daodao
# @FileName: work11.py
# @Software: PyCharm
# @qq ：402583874

'''
面向对象一
1、类属性怎么定义？ 实例属性怎么定义？什么属性适合定义为类属性，什么属性适合定义成实例属性（简答）

2、实例方法中的self代表什么？（简答）

3、类中__init__方法在什么时候调用的？（简答）

4、封装一个学生类，(自行分辨定义为类属性还是实例属性，方法定义为实例方法)
-  属性：身份(学生)，姓名，年龄，性别，英语成绩，数学成绩，语文成绩，
-  方法一：计算总分，方法二：计算三科平均分，方法三：打印学生的个人信息。

5、封装一个测试用例类(自行分辨定义为类属性还是实例属性)，
-  属性：用例编号  url地址   请求参数   请求方法    预期结果   实际结果
'''
'''
第一题：
1、类的属性定义：直接在类里面定义的变量，就叫类的属性
   实例属性定义：对象.属性名=属性值
   类属性：这类事物所有的对象都具有这个属性，属性值是一样的，适合定义为类属性
   实例属性：这里事物所有的对象都具有这个属性，但是属性值不一样，适合定义为实例属性
2、实例方法中self代表对象本身，哪个对象调用方法就代表那个对象调用方法
3、类中__init__方法在创建实例对象时调用

'''
# #-------------------第四题-----------------------
#
# class Students():
#     '''学生类'''
#     identity='学生类'
#
#     def __init__(self,name,age,gender,english,number,chinese):
#         self.name=name
#         self.age=age
#         self.gender=gender
#         self.english=english
#         self.number=number
#         self.chinese=chinese
#
#     def sum_score(self):
#         res=self.english+self.number+self.chinese
#         return res
#     def avg_score(self):
#         res = (self.english + self.number + self.chinese)/3
#         return res
#     def print_info(self):
#         print(f'学员姓名：{self.name}，年龄：{self.age}，性别{self.gender}')
#
# s1 = Students("小明", 18, "男", 80, 90, 88)
# g=s1.sum_score()
# print(g)
# a=s1.avg_score()
# print(a)
# s1.print_info()

#-------------------第五题-----------------------

class Cases:
    """用例类"""

    def __init__(self, case_id, url, data, method, excepted, actual):
        self.case_id = case_id
        self.url = url
        self.data = data
        self.method = method
        self.excepted = excepted
        self.actual = actual

# 类属性：类和实例对象都可以调用
# 实例属性：只要实例对象才能够调用