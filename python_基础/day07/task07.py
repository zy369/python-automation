# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 15:52
# @Author  : daodao
# @FileName: task07.py
# @Software: PyCharm
# @qq ：402583874
'''
第一题：简单题
 1、什么是全局变量？ 答：直接定义在py文件中的变量叫全局变量
 2、什么是局部变量？ 答： 定义在函数内部的变量叫局部变量
 3、函数内部如何修改全局变量（如何声明全局变量 ）？ 答 global 在函数内部声明全局变量
 4、写出已经学过的所有python关键字，分别写出用途？
        答 ：学过的20个
False:bool 数据类型
True: bool 数据类型
None:表示数据为空

and  逻辑运算符 与
or   逻辑运算符：或
not  逻辑运算符：非

is： 身份运算符
in:  成员运算符
del: 删除数据
pass: 表示通过（一般用来占位的）

if: 条件判断
elif: 条件判断
else: 条件判断

while: 条件循环
for  : 遍历循环
break: 用来终止循环
continue: 终止当前本轮循环 ，开启下一轮循环

def : 定义函数
return: 函数返回值
global: 定义全局变量

第二题：数据转换
现在有以下数据， li1 = ["{'a':11,'b':2}","[11,22,33,44]"]
需要转换为以下格式： li1 = [{'a':11,'b':2},[11,22,33,44]]
请封装一个函数，按上述要求实现数据的格式转换
第三题：数据转换
# 有一组用例数据如下：
cases = [
    ['case_id', 'case_title', 'url', 'data', 'excepted'],
    [1, '用例1', 'www.baudi.com', '001', 'ok'],
    [4, '用例4', 'www.baudi.com', '002', 'ok'],
    [2, '用例2', 'www.baudi.com', '002', 'ok'],
    [3, '用例3', 'www.baudi.com', '002', 'ok'],
    [5, '用例5', 'www.baudi.com', '002', 'ok'],
]

# 要求一：把上述数据转换为以下格式
res1 = [
    {'case_id': 1, 'case_title': '用例1', 'url': 'www.baudi.com', 'data': '001', 'excepted': 'ok'},
    {'case_id': 4, 'case_title': '用例4', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 2, 'case_title': '用例2', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 3, 'case_title': '用例3', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 5, 'case_title': '用例5', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'}
]
# 要求二：把上面转换好的数据中case_id大于3的用例数据获取出来,得到如下结果
res = [
    {'case_id': 4, 'case_title': '用例4', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'},
    {'case_id': 5, 'case_title': '用例5', 'url': 'www.baudi.com', 'data': '002', 'excepted': 'ok'}
]
'''
# print('第二题数据转换')
# li1 = ["{'a':11,'b':2}","[11,22,33,44]"]
# def work2(a):
#     new_li= []
#     for i in li1:
#         new_li.append(eval(i))
#     return new_li
#
# res=work2(li1)
# print(res)

cases = [
    ['case_id', 'case_title', 'url', 'data', 'excepted'],
    [1, '用例1', 'www.baudi.com', '001', 'ok'],
    [4, '用例4', 'www.baudi.com', '002', 'ok'],
    [2, '用例2', 'www.baudi.com', '002', 'ok'],
    [3, '用例3', 'www.baudi.com', '002', 'ok'],
    [5, '用例5', 'www.baudi.com', '002', 'ok'],
]

def work3_1(cases):
    new_data = []
    for case in cases[1:]:
        new_data.append(dict(zip(cases[0],case)))
    return new_data
res1=work3_1(cases)
# print(res1)
def work3_2(data):
    new_data=[]
    for i in data:
        if i['case_id']>3:
           new_data.append(i)
    return new_data
res=work3_2(res1)
print(res)