# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 15:17
# @Author  : daodao
# @FileName: 05debug 定位问题.py
# @Software: PyCharm
# @qq ：402583874

'''
print ：调试
打断点：断点打在报错的那一行上，或者之前都可以
'''
a=1
b='1'
def func():
    c=a+b
    print(c)
func()
