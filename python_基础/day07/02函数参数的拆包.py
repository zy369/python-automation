# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 14:45
# @Author  : daodao
# @FileName: 02函数参数的拆包.py
# @Software: PyCharm
# @qq ：402583874

def func(a,b):
    print('a=',a)
    print('b=',b)
# 传递参数时，可以用* 对列表，元组,集合拆包
# li=[11,22]
# func(*li)
# tu=(44,55)
# func(*tu)
# set1={66,77}
# func(*set1)
# 传递参数时，可以用** 对字典拆包
dic={"a":11,"b":22}
func(**dic)

tuple2=(1,2)
a,b=tuple2
print(a,type(a))
print(b,type(b))