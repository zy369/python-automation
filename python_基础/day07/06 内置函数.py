# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 15:27
# @Author  : daodao
# @FileName: 06 内置函数.py
# @Software: PyCharm
# @qq ：402583874

'''
len() 获取 字符串\列表\元组\字典中的元素总数量（数据的长度）
max() 获取数据元素中的最大值
min() 获取数据元素中的最小值
sum() 对元素进行求和
eval() 识别字符串中的python表达式
zip() 聚合打包
'''
# len() 获取 字符串\列表\元组\字典中的元素总数量（数据的长度）
s1 = "asdfoifjashf"
print(len(s1))
li=[11,33,44]
print(len(li))
dic={'a':11,'b':22}
print(len(dic))

# max() 获取数据元素中的最大值
print(max(li))
# min() 获取数据元素中的最小值
print(min(li))
# sum() 对元素进行求和
print(sum(li))
# eval 识别字符串中的python表达式

li="[1,2,3]"
res=eval(li)  #==> [1,2,3]
print(res)
s2='print("python666")'
eval(s2)
# zip 聚合打包
li=['name','age','gender','666']
li2=['daodao',18,'男']
res = zip(li,li2)
# print(list(res))
# 把上面的元素转换成字典，li中元素作为键，li2中的 元素作为值
print(dict(res))