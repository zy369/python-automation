# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 13:51
# @Author  : daodao
# @FileName: 01不定长参数.py
# @Software: PyCharm
# @qq ：402583874
'''
不定长参数（动态参数，可变参数）
    *args  定义的时候参数名前加一个*(通常定义为*args),可以接受0个或多个位置传参，保存为一个元组
    **kwargs 定义的时候参数名前加两个**（通常定义为**kwargs）,可以接受0个或多个关键字传参，保存为一个字典

'''
def func(*args,**kwargs):
    print(args)
    print(type(args))
    print(kwargs)
    print(type(kwargs))
func(1,2)
func(a=1,b=2)
