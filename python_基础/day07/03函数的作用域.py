# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 14:57
# @Author  : daodao
# @FileName: 03函数的作用域.py
# @Software: PyCharm
# @qq ：402583874

'''
全局变量：直接定义在py文件中的变量，叫全家变量，在该文件中任何地方都可以使用
局部变量：在函数内部定义的变量，叫局部变量，只能在该函数内部使用，函数外部无法使用
变量的查找过程：由内到外（先找自身，没有再去外部寻找）
如何再函数内部修改全局变量？
    global:再函数内部声明全局变量
'''

# 定义一个全局变量
name='daodao'

# def func():
#     # 定义一个局部变量
#     a=1
#     print(a)
#     print(name)
# func()
# print(name)

'''
变量的查找过程：由内到外（先找自身，没有再去外部寻找）
'''
# a=10
# def func():
#     #a=1
#     print(a)
# func()
'''
如何再函数内部修改全局变量？
    global:再函数内部声明全局变量
'''

a=10
def func():
    global a
    a+=1
    print(a)
func()
print(a)

