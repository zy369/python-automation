# -*- coding: utf-8 -*-
# @Time    : 2021/5/28 15:10
# @Author  : daodao
# @FileName: 04pycharm的debug调试.py
# @Software: PyCharm
# @qq ：402583874

'''
pycharm 自带的debug调试工具
F8：按一下 往下执行一行代码（遇到函数调用，直接运行函数的结果，不会进入函数内部）
F7：按一下 往下执行一行代码（遇到函数调用会进入内部）
'''
name = 'daodao'
age = 18
li1=[11,22,33]

def func():
    aa=100
    bb=200
    print(aa)
    print(bb)
print(name)
func()