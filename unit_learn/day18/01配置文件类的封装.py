# -*- coding: utf-8 -*-
# @Time    : 2021/6/9 16:28
# @Author  : daodao
# @FileName: 01配置文件类的封装.py
# @Software: PyCharm
# @qq ：402583874

from configparser import ConfigParser

class MyConf(object):

    def __init__(self,filename,encoding='utf-8'):
        '''

        :param filename:配置文件名
        :param encoding:文件编码方式
        '''
        self.filename=filename
        self.encoding=encoding
        self.conf=ConfigParser()
        self.conf.read(filename,encoding=encoding)

    def get_str(self,section,option):
        return self.conf.get(section,option)

    def get_int(self,section,option):
        return self.conf.getint(section,option)

    def get_float(self,section,option):
        return self.conf.getfloat(section,option)

    def get_boolean(self,section,option):
        return self.conf.getboolean(section,option)

    def write(self,section,option,value):
        self.conf.set(section,option,value)
        self.write(open(self.filename,'w',encoding=self.encoding))

class MyConf2(ConfigParser):

    def __init__(self,filename,encoding='utf-8'):
        super().__init__()
        self.filename=filename
        self.encoding=encoding
        self.read(filename,encoding)

    def write_data(self,section,option,value):
        self.set(section,option,value)
        self.write(open(self.filename,'w',encoding=self.encoding))


if __name__ == '__main__':
    # conf=MyConf('conf.ini')
    # res=conf.get_str('daodao','age')
    # print(res)
    conf2=MyConf2('conf.ini')
    res2=conf2.write_data('daodao','bbb','大佬')