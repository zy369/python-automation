# -*- coding: utf-8 -*-
# @Time    : 2021/6/11 16:53
# @Author  : daodao
# @FileName: 03json_test.py
# @Software: PyCharm
# @qq ：402583874
import json
data = {"name":"musen","id":18,"msg":None}
json_data = '{"name":"musen","id":19,"msg":null}'
# json.loads  将字符串转化成python类型，null转化成None
res=json.loads(json_data)
print(res,type(res))
# json.dumps 将python类型转化成字符串，None转化成null
res=json.dumps(data)
print(res,type(res))