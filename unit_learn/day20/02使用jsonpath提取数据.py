# -*- coding: utf-8 -*-
# @Time    : 2021/6/11 15:10
# @Author  : daodao
# @FileName: 02使用jsonpath提取数据.py
# @Software: PyCharm
# @qq ：402583874

import requests
import jsonpath

login_url="http://api.lemonban.com/futureloan/member/login"

header = {
    "X-Lemonban-Media-Type": "lemonban.v2",
    "Content-Type": "application/json"
}

login_data={
    "mobile_phone": "18888888844",
    "pwd": "12345678"
}
response=requests.post(url=login_url,json=login_data,headers=header)
json_data=response.json()

member_id=jsonpath.jsonpath(json_data,'$..id')[0]
type_token=jsonpath.jsonpath(json_data,'$..token_type')[0]
token=jsonpath.jsonpath(json_data,'$..token')[0]
token_data=type_token +' ' + token
header_token = {
    "X-Lemonban-Media-Type": "lemonban.v2",
    "Content-Type": "application/json",
    "Authorization":token_data
}

recharge_data = {
    "member_id":member_id,
    "amount":2000
}
recharge_url="http://api.lemonban.com/futureloan/member/recharge"
response=requests.post(url=recharge_url,json=recharge_data,headers=header_token)
print(response.json())

