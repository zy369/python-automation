# -*- coding: utf-8 -*-
# @Time    : 2021/6/11 17:02
# @Author  : daodao
# @FileName: 04通过cookie和session鉴权的接口处理方式.py
# @Software: PyCharm
# @qq ：402583874

import requests

#  --------------------------------使用requests直接发生请求，无法通过session鉴权--------------------------------------
# 老版接口
# register_url = "http://test.lemonban.com/futureloan/mvc/api/member/register"
# data = {
#     "mobilephone": "18888888885",
#     "pwd":"12345678"
# }
# response = requests.get(url=register_url, params=data)
# print(response.json())

# login_url="http://test.lemonban.com/futureloan/mvc/api/member/login"
# data = {
#     "mobilephone": "18888888885",
#     "pwd":"12345678"
# }
# res=requests.post(login_url,data)
# print(res.json())
# print(res.cookies)

# recharge_url="http://test.lemonban.com/futureloan/mvc/api/member/recharge"
# data = {
#     "mobilephone": "18888888885",
#     "amount":"2000"
# }
# res=requests.post(recharge_url,data)
# print(res.json())
# print(res.cookies)

#-------------------使用requests模块中的session对象发送请求----------------------------
# session()能自动记录上一次的cookie信息
se=requests.session()
login_url="http://test.lemonban.com/futureloan/mvc/api/member/login"
data = {
    "mobilephone": "18888888885",
    "pwd":"12345678"
}
res=se.post(login_url,data)
print(res.json())
print(res.cookies)

recharge_url="http://test.lemonban.com/futureloan/mvc/api/member/recharge"
data = {
    "mobilephone": "18888888885",
    "amount":"2000"
}
res=se.post(recharge_url,data)
print(res.json())
print(res.cookies)