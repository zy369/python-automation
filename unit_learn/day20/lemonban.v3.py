# -*- coding: utf-8 -*-
# @Time    : 2021/6/15 10:19
# @Author  : daodao
# @FileName: lemonban.v3.py
# @Software: PyCharm
# @qq ：402583874
import requests
import time
import base64
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher_pkcs1_v1_5

login_url="http://api.lemonban.com/futureloan/member/login"
header = {
    "X-Lemonban-Media-Type": "lemonban.v3",
    "Content-Type": "application/json"
}

login_data={
    "mobile_phone": "18888888844",
    "pwd": "12345678"
}
response=requests.post(url=login_url,json=login_data,headers=header)
json_data=response.json()
member_id=json_data['data']['id']
timestamp=int(time.time())
type_token=json_data['data']['token_info']['token_type']
token=json_data['data']['token_info']['token']
Bearer_token=type_token + ' ' + token
stimestamp_token_sign=token[:50]+ str(timestamp)

def rsaEncrypt(msg):
    """
    公钥加密
    :param msg: 要加密内容
    :type msg:str
    :return: 加密之后的密文
    """
    key = '''-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQENQujkLfZfc5Tu9Z1LprzedE
O3F7gs+7bzrgPsMl29LX8UoPYvIG8C604CprBQ4FkfnJpnhWu2lvUB0WZyLq6sBr
tuPorOc42+gLnFfyhJAwdZB6SqWfDg7bW+jNe5Ki1DtU7z8uF6Gx+blEMGo8Dg+S
kKlZFc8Br7SHtbL2tQIDAQAB
-----END PUBLIC KEY-----'''
    publickey = RSA.importKey(key)
    cipher = Cipher_pkcs1_v1_5.new(publickey)
    # 分段加密
    cipher_text = []
    for i in range(0, len(msg), 80):
        cont = msg[i:i + 80]
        cipher_text.append(cipher.encrypt(cont.encode()))
    # base64 进行编码
    cipher_text = b''.join(cipher_text)
    cipher_result = base64.b64encode(cipher_text)
    # 返回密文
    return cipher_result.decode()
    # 加密操作
en_msg = rsaEncrypt(msg=stimestamp_token_sign)
print('加密密文：', en_msg)
header_token = {
    "X-Lemonban-Media-Type": "lemonban.v3",
    "Content-Type": "application/json",
    "Authorization":Bearer_token
}

recharge_data = {
    "member_id":member_id,
    "amount":2000,
    "sign":en_msg,
    "timestamp":timestamp
}
recharge_url="http://api.lemonban.com/futureloan/member/recharge"
response=requests.post(url=recharge_url,json=recharge_data,headers=header_token)
print(response.json())
print()
