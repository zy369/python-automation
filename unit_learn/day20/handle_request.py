# -*- coding: utf-8 -*-
# @Time    : 2021/6/15 13:25
# @Author  : daodao
# @FileName: handle_request.py
# @Software: PyCharm
# @qq ：402583874

'''
封装的目的
封装的需求
    发送post请求，发送get请求，发送patch请求
    代码中如何根据不同请求方式的接口发送不同请求
    加判断
'''
import  requests

class HandleRequests:

    def send(self,url,method,params=None,data=None,json=None,headers=None):

        if method=='POST':
            return requests.post(url=url,json=json,data=data,headers=headers)
        elif method=='PATCH':
            return requests.patch(url=url, json=json, data=data, headers=headers)
        elif method=='GET':
            return requests.get(url=url,params=params)

class HandleSessionRequests:

    def __init__(self):
        self.se=requests.session()

    def send(self,url,method,params=None,data=None,json=None,headers=None):

        if method=='POST':
            return self.se.post(url=url,json=json,data=data,headers=headers)
        elif method=='PATCH':
            return self.se.patch(url=url, json=json, data=data, headers=headers)
        elif method=='GET':
            return self.se.get(url=url,params=params)

if __name__ == '__main__':
    login_url="http://api.lemonban.com/futureloan/member/login"

    header = {
        "X-Lemonban-Media-Type": "lemonban.v2",
        "Content-Type": "application/json"
    }

    login_data={
        "mobile_phone": "18888888844",
        "pwd": "12345678"
    }
    # response=HandleRequests().send(url=login_url,json=login_data,headers=header,method='POST')
    response=HandleSessionRequests().send(url=login_url,json=login_data,headers=header,method='POST')
    json_data=response.json()
    print(json_data)