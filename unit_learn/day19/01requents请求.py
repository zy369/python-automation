# -*- coding: utf-8 -*-
# @Time    : 2021/6/11 11:09
# @Author  : daodao
# @FileName: 01requents请求.py
# @Software: PyCharm
# @qq ：402583874
'''
requests
get
post
json类型数据
表单类型数据
上传文件


json()
text()
content.decode()

'''
import requests

url="http://8.129.91.152:8766/futureloan/member/register"
register_url='http://api.lemonban.com/futureloan/member/register'

header={"X-Lemonban-Media-Type": "lemonban.v1",
        "Content-Type": "application/json"}
data={
    "mobile_phone": "18888888844",
    "pwd": "12345678",
    "reg_name": "daodao",
    "type": 0
}

# json类型的数据
# response=requests.post(url=register_url,json=data,headers=header)
# print(response.json())
# print(response.text)
# print(response.content.decode('utf-8'))
# 表单类型的数据
response=requests.post(url=register_url,data=data,headers=header)
print(response.text)
# 上传文件
# response=requests.post(url=register_url,files=None)
# print(response.content.decode('utf-8'))
# print(type(response.json()))

# get请求
# get_url="http://test.lemonban.com/futureloan/mvc/api/member/register"
# get_data = {
#     "mobilephone": "18888888884",
#     "pwd":"123qwe"
# }
# response = requests.get(url=get_url, params=get_data)
# print(response.json())

# patch
# patch_url='http://api.lemonban.com/futureloan/member/update'
# patch_headers = {
#     "X-Lemonban-Media-Type": "lemonban.v1",
#     "Content-Type": "application/json"
# }
# patch_data = {
#     "member_id": 1234599691,
#     "reg_name": "daodao888"
# }
#
# response=requests.patch(url=patch_url,json=patch_data,headers=patch_headers)
# print(response.json())
