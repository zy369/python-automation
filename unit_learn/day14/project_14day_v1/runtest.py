# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 16:42
# @Author  : daodao
# @FileName: runtest.py
# @Software: PyCharm
# @qq ：402583874
import unittest

from unit_learn.day14.project_14day_v1.HTMLTestRunnerNew import HTMLTestRunner
from unit_learn.day14.project_14day_v1.readexcel import ReadExcel
from unit_learn.day14.project_14day_v1.testcases import LoginTestCase

suite=unittest.TestSuite()

# 将测试用例加载到测试套件中
exel=ReadExcel("cases.xlsx","login")
cases=exel.read_data()
for item in cases:
    case=LoginTestCase("test_login",eval(item['data']),eval(item['expected']))
    suite.addTest(case)

with open('report.html','wb') as fb:
    runner=HTMLTestRunner( stream=fb,
                           title="python单元测试报告",
                           description="python单元测试报告，用HTMLTestRunner生成测试报告",
                           tester="daodao,小明，大哥")
    runner.run(suite)