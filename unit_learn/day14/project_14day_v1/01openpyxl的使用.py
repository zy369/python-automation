# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 15:45
# @Author  : daodao
# @FileName: 01openpyxl的使用.py
# @Software: PyCharm
# @qq ：402583874

import openpyxl

# 第一步打开工作簿
workbook=openpyxl.load_workbook(r'E:\python-automation\unit_learn\day14\project_14day_v1\cases.xlsx')

# 第二步 选择表单对象
sheet=workbook["login"]

# 第三步 通过表单对象选中表格读取数据
##  1、读取内容
data=sheet.cell(row=5,column=1)
# print(data.value)
## 2、写入内容
sheet.cell(row=7,column=3,value='(",")')

## 写入内容后一定要保存才能生效
workbook.save(r'E:\python-automation\unit_learn\day14\project_14day_v1\cases.xlsx')

## 获取最大行和最大列
print(sheet.max_row)
print(sheet.max_column)
# 注意点不要随意在表格中敲空格
# rows 按行获取格子对象，每一行的格子对象放入一个元组中
print(list(sheet.rows))
