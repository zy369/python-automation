# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 16:13
# @Author  : daodao
# @FileName: 02openpyxl的封装.py
# @Software: PyCharm
# @qq ：402583874
'''
为什么要做封装？
    为了使用方法，提高代码的复用率
封装的需求
1、读取数据的方法
2、写入数据的方法
'''
import openpyxl
class ReadExcel(object):

    def __init__(self,filename,sheet_name):
        '''

        :param filename:文件名
        :param sheet_name:表单名
        '''
        self.filename=filename
        self.sheet_name=sheet_name

    def open(self):
        self.wb=openpyxl.load_workbook(self.filename)
        self.sh=self.wb[self.sheet_name]

    def save(self):
        self.wb.save(self.filename)

    def read_data(self):
        '''读取数据'''
        self.open()
        max_row=self.sh.max_row
        list_data=[]
        for i in range(1,max_row+1):
            data1 = self.sh.cell(row=i, column=1).value
            data2 = self.sh.cell(row=i, column=2).value
            data3 = self.sh.cell(row=i, column=3).value
            data4 = self.sh.cell(row=i, column=4).value
            list_data.append([data1,data2,data3,data4])
        cases=[]
        title=list_data[0]
        for  data in list_data[1:]:
            case=dict(zip(title,data))
            cases.append(case)
        return cases

if __name__ == '__main__':
    exel=ReadExcel(r'E:\python-automation\unit_learn\day14\project_14day_v1\cases.xlsx','login')
    cases=exel.read_data()
    print(cases)
