# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 16:32
# @Author  : daodao
# @FileName: testcases.py
# @Software: PyCharm
# @qq ：402583874
import unittest
from unit_learn.day14.project_14day_v1.login import login_check
class LoginTestCase(unittest.TestCase):

    def __init__(self,methodName,data,expected):
        super().__init__(methodName)
        self.data=data
        self.expected=expected

    def test_login(self):
        res=login_check(*self.data)
        self.assertEqual(self.expected,res)

if __name__ == '__main__':
    unittest.main()

