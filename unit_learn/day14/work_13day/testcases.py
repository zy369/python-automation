# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 15:16
# @Author  : daodao
# @FileName: testcases.py
# @Software: PyCharm
# @qq ：402583874

import unittest
from unit_learn.day14.work_13day.register import Register

class RegisterTestCase(unittest.TestCase):

    def test_register_pass(self):

        expected={"code": 1, "msg": "注册成功"}
        data=('python1','123456','123456')
        res= Register(*data)
        self.assertEqual(expected,res)

    def test_register_password_dif(self):
        expected={"code": 0, "msg": "两次密码不一致"}
        data=('python2','12345','123456')
        res=Register(*data)
        self.assertEqual(expected,res)