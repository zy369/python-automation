# -*- coding: utf-8 -*-
# @Time    : 2021/6/9 16:00
# @Author  : daodao
# @FileName: 01配置文件的操作.py
# @Software: PyCharm
# @qq ：402583874
'''
创建对象
读取文件
get获取内容
getint 读取int型数据
getfloat 获取float型的数据
getboolean 获取boolean型的数据
set 修改配置文件中的值
write 写入文件
'''
from configparser import ConfigParser

conf=ConfigParser()
c=conf.read('conf.ini',encoding='utf-8')
res=conf.get('logging','level')
print(res)
res2=conf.getint('daodao','age')
print(res2,type(res2))
res3=conf.getfloat('daodao','money')
print(res3,type(res3))
res4=conf.getboolean('daodao','switch')
print(res4,type(res4))
conf.set('daodao','aaa','python666')
conf.write(open('conf.ini','w',encoding='utf-8'))