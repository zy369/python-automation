# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 17:15
# @Author  : daodao
# @FileName: 01日志模块.py
# @Software: PyCharm
# @qq ：402583874

import  logging
# 获取日志记录器
my_log=logging.getLogger()
my_log.setLevel('DEBUG')

a=100
my_log.debug(a)
logging.info('INFO级别日志')
logging.warning('WARNING级别日志记录')
logging.error('ERROR级别日志记录')
logging.critical('CRITICAL级别日志记录')
logging.debug('DEBUG级别日志')