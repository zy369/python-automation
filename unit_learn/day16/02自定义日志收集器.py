# -*- coding: utf-8 -*-
# @Time    : 2021/6/9 15:24
# @Author  : daodao
# @FileName: 02自定义日志收集器.py
# @Software: PyCharm
# @qq ：402583874
''''
日志级别从低到高
 1. DEBUG
 2. INFO
 3. WARNING
 4. ERROR
 5. CRITICAL
'''
import logging

# 一、创建日志收集器
my_log=logging.getLogger('daodao')
#二、 设置级别
my_log.setLevel('DEBUG')
#三、输入出渠道
## 输出到控制台
sh=logging.StreamHandler()
sh.setLevel('ERROR')
my_log.addHandler(sh)
## 输出到文件中
fh=logging.FileHandler('log.log',encoding='utf-8')
fh.setLevel('DEBUG')
my_log.addHandler(fh)
# 四、设置日志输出格式
formatter = logging.Formatter('%(asctime)s - [%(filename)s-->line:%(lineno)d] - %(levelname)s: %(message)s')
sh.setFormatter(formatter)
fh.setFormatter(formatter)

my_log.debug('这是debug')
my_log.info('这是info')
my_log.warning('这是warning')
my_log.error('这是error')
my_log.critical('这是critical')