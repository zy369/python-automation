# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 8:48
# @Author  : daodao
# @FileName: readexcel.py
# @Software: PyCharm
# @qq ：402583874

'''

'''
import openpyxl
# 用来保存用例数据
class CaseData:
    pass

class ReadExcel(object):

    def __init__(self,filename,sheet_name):

        self.filename=filename
        self.sheet_name=sheet_name

    def open(self):
        self.wookbook=openpyxl.load_workbook(self.filename)
        self.sheet=self.wookbook[self.sheet_name]

    def close(self):
        self.wookbook.close()

    def read_data(self):
        self.open()
        rows=list(self.sheet.rows)
        title=[]
        for r in rows[0]:
            title.append(r.value)
        cases=[]
        for row in rows[1:]:
            data = []
            for r in row:
                data.append(r.value)
            case=dict(zip(title,data))
            cases.append(case)
        self.close()
        return cases

    def read_data_obj(self):

        self.open()
        rows=list(self.sheet.rows)
        title=[]
        for i in rows[0]:
            title.append(i.value)
        cases=[]
        for row in rows[1:]:
            data=[]
            for r in row:
                data.append(r.value)
            case=list(zip(title,data))
            case_obj=CaseData()
            for k,v in case:
                setattr(case_obj,k,v)
            cases.append(case_obj)
        self.close()
        return cases

    def write_data(self,row,column,value):

        self.open()
        self.sheet.cell(row=row,column=column,value=value)
        self.wookbook.save(self.filename)
        self.close()

if __name__ == '__main__':
    excel=ReadExcel('cases.xlsx','login')
    cases=excel.read_data()
    print(cases)
    # cases=excel.read_data_obj()
    # print(cases)
    # excel.write_data(11,3,'我')


