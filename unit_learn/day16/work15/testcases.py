# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 13:54
# @Author  : daodao
# @FileName: testcases.py
# @Software: PyCharm
# @qq ：402583874
import unittest

from unit_learn.day16.work15.ddt import ddt, data
from unit_learn.day16.work15.login import login_check
from unit_learn.day16.work15.readexcel import ReadExcel

@ddt
class LoginTestCase(unittest.TestCase):

    excel=ReadExcel('cases.xlsx','login')
    cases=excel.read_data()

    @data(*cases)
    def test_login(self,case):
        case_data=eval(case['data'])
        expected=eval(case['expected'])
        case_id=case['case_id']
        result=login_check(*case_data)
        try:
            self.assertEqual(expected,result)
        except AssertionError as e:
            self.excel.write_data(row=case_id+1,column=5,value='failure')
            raise e
        else:
            self.excel.write_data(row=case_id + 1, column=5, value='pass')