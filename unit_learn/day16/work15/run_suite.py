# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 14:09
# @Author  : daodao
# @FileName: run_suite.py
# @Software: PyCharm
# @qq ：402583874

import unittest

from unit_learn.day16.work15 import testcases
from unit_learn.day16.work15.HTMLTestRunnerNew import HTMLTestRunner

suite=unittest.TestSuite()
loader=unittest.TestLoader()
suite.addTest(loader.loadTestsFromModule(testcases))

with open('report.html','wb') as fb:
    runner=HTMLTestRunner( stream=fb,
                           title="python单元测试报告",
                           description="python单元测试报告，用HTMLTestRunner生成测试报告",
                           tester="daodao,小明，大哥")
    runner.run(suite)

