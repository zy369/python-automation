# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 17:09
# @Author  : daodao
# @FileName: testcases.py
# @Software: PyCharm
# @qq ：402583874
import unittest

from unit_learn.day15.work_14.login import login_check
from unit_learn.day15.work_14.register import register


class LoginTestCase(unittest.TestCase):

    def __init__(self,methodName,data,expected):
        super().__init__(methodName)
        self.data=data
        self.expected=expected

    def test_login(self):
        res=login_check(*self.data)
        self.assertEqual(self.expected,res)

class RegisterTestCase(unittest.TestCase):
    def __init__(self,methodName,data,expected):
        super().__init__(methodName)
        self.data=data
        self.expected=expected

    def test_register(self):
        res=register(*self.data)
        self.assertEqual(self.expected,res)
