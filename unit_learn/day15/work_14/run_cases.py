# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 17:15
# @Author  : daodao
# @FileName: run_cases.py
# @Software: PyCharm
# @qq ：402583874

import unittest


from unit_learn.day15.work_14.HTMLTestRunnerNew import HTMLTestRunner
from unit_learn.day15.work_14.testcases import LoginTestCase, RegisterTestCase
from unit_learn.day15.work_14.readexcel import ReadExcel

suite=unittest.TestSuite()

excel=ReadExcel('cases.xlsx','login')
cases=excel.read_data()
for item in cases:
    case=LoginTestCase("test_login",eval(item['data']),eval(item['expected']))
    suite.addTest(case)

excel=ReadExcel('cases.xlsx','register')
cases=excel.read_data()
for item in cases:
    case=RegisterTestCase("test_register",eval(item["data"]),eval(item["expected"]))
    suite.addTest(case)

with open('report.html','wb') as fb:
    runner=HTMLTestRunner( stream=fb,
                           title="python单元测试报告",
                           description="python单元测试报告，用HTMLTestRunner生成测试报告",
                           tester="daodao,小明，大哥")
    runner.run(suite)