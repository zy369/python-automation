# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 9:54
# @Author  : daodao
# @FileName: testcases.py
# @Software: PyCharm
# @qq ：402583874
import unittest

from unit_learn.day15.pay15day_project.login import login_check
from unit_learn.day15.pay15day_project.readexcel import ReadExcel
from unit_learn.day15.pay15day_project.register import register

excel=ReadExcel('cases.xlsx','login')
excel2=ReadExcel('cases.xlsx','register')

class LoginTestCase(unittest.TestCase):

    def __init__(self,methodName,data,expected,case_id):

        super().__init__(methodName)
        self.data=data
        self.expected=expected
        self.case_id=case_id

    def test_login(self):

        result=login_check(*self.data)
        try:
            self.assertEqual(self.expected,result)
        except AssertionError as e:
            excel.write_data(row=self.case_id+1,column=5,value='fail')
            raise e
        else:
            excel.write_data(row=self.case_id + 1, column=5, value='pass')

class RegisterTestCase(unittest.TestCase):

    def __init__(self,methodName,data,expected,case_id):

        super().__init__(methodName)
        self.data=data
        self.expected=expected
        self.case_id=case_id

    def test_register(self):
        try:
            result=register(*self.data)
        except AssertionError as e:
            excel2.write_data(row=self.case_id+1,column=5,value='failure')
        else:
            excel2.write_data(row=self.case_id+1,column=5,value='pass')