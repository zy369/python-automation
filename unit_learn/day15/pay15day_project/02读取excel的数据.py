# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 8:38
# @Author  : daodao
# @FileName: 02读取excel的数据.py
# @Software: PyCharm
# @qq ：402583874
import openpyxl

# 打开工作簿
workbook=openpyxl.load_workbook('cases.xlsx')
# 选中表单
sheet=workbook['register']
# 按行获取格子对象，每行对象放在元组中
rows=list(sheet.rows)
title=[]
for i in rows[0]:
    title.append(i.value)
cases=[]
for row in rows[1:]:
    data = []
    for r in row:
        data.append(r.value)
    case=dict(zip(title,data))
    cases.append(case)
print(cases)

tu=(11,222,333 ,44)
k,v,c,g=tu
print(k)
print(v)

