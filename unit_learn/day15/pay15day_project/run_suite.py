# -*- coding: utf-8 -*-
# @Time    : 2021/6/8 10:12
# @Author  : daodao
# @FileName: run_suite.py
# @Software: PyCharm
# @qq ：402583874

'''

'''
import unittest

from unit_learn.day15.pay15day_project.HTMLTestRunnerNew import HTMLTestRunner
from unit_learn.day15.pay15day_project.readexcel import ReadExcel
from unit_learn.day15.pay15day_project.testcases import LoginTestCase, RegisterTestCase

suite=unittest.TestSuite()

excel = ReadExcel('cases.xlsx','login')
excel2 = ReadExcel('cases.xlsx','register')
cases=excel.read_data()
re_cases=excel2.read_data()
for item in cases:
    case=LoginTestCase("test_login",eval(item["data"]),eval(item["expected"]),item["case_id"])
    suite.addTest(case)

for elem in  re_cases:
    case=RegisterTestCase('test_register',eval(elem['data']),eval(elem['expected']),elem['case_id'])
    suite.addTest(case)

with open('report.html','wb') as fb:
    runner=HTMLTestRunner( stream=fb,
                           title="python单元测试报告",
                           description="python单元测试报告，用HTMLTestRunner生成测试报告",
                           tester="daodao,小明，大哥")
    runner.run(suite)

