# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 13:30
# @Author  : daodao
# @FileName: testcases.py
# @Software: PyCharm
# @qq ：402583874

'''
测试用例类：自己定义的类只要继承unittest 中的TestCase类，那么这个类就是测试用例类
测试用例：测试用例类中已test开头的方法，就是一条测试用例
用例执行通过没通过的评判标准：断言异常
'''
from unitstuy.day13.login import login_check
import unittest
class LoginTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        # 测试用例类开始执行时执行
        print("------------setUpClass---------------------\n")
    @classmethod
    def tearDownClass(cls) -> None:
        # 测试用例类结束时开始执行
        print('------------tearDownClass---------------')

    def setUp(self) -> None:
        #每条用例执行之前执行
        print(f'用例{self}开始执行')

    def tearDown(self) -> None:
        #每条用例执行结束后执行
        print(f'用例{self}执行结束 ')

    def test_login_pass(self):

        # 第一步准备用例数据
        # 1、用例的参数
        data=('python','123')
        # 2、预期结果
        expected={"code":0,"msg":"登录成功"}
        # 第二步、执行功能函数，获取实际结果
        result=login_check(*data)
        # 第三步 比对预期结果和实际结果
        self.assertEqual(expected,result)

    def test_login_pwd_error(self):

        # 第一步准备用例数据
        # 1、用例的参数
        data=('python','1234')
        # 2、预期结果
        expected={"code": 1, "msg": "账号或密码不正确"}
        # 第二步、执行功能函数，获取实际结果
        result=login_check(*data)
        # 第三步 比对预期结果和实际结果
        self.assertEqual(expected,result)
    # def login_pass_test(self):
    #     # 第一步准备用例数据
    #     # 1、用例的参数
    #     data = ('python', '123')
    #     # 2、预期结果
    #     expected = {"code": 0, "msg": "登录成功"}
    #     # 第二步、执行功能函数，获取实际结果
    #     result = login_check(*data)
    #     # 第三步 比对预期结果和实际结果
    #     self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()
