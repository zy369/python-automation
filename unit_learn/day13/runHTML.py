# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 14:14
# @Author  : daodao
# @FileName: runHTML.py
# @Software: PyCharm
# @qq ：402583874

import unittest
from unit_learn.day13 import testcases
from unit_learn.day13.HTMLTestRunnerNew import  HTMLTestRunner

# 第一步 创建一个测试套件
suite=unittest.TestSuite()

# 第二步 讲测试用例加载进测试套件，按照模块加载
loader=unittest.TestLoader()
suite.addTest(loader.loadTestsFromModule(testcases))

# 第三步 创建一个测试运行程序启动器
runner=HTMLTestRunner( stream=open('report.html','wb'),
                       title="python单元测试报告",
                       description="python单元测试报告，用HTMLTestRunner生成测试报告",
                       tester="daodao,小明，大哥")
# 第四步 使用启动器去启动测试套件
runner.run(suite)