# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 13:53
# @Author  : daodao
# @FileName: runtest.py
# @Software: PyCharm
# @qq ：402583874
'''
这个文件是测试程序运行的启动文件
'''
import unittest

# 第一步创建一个测试套件
suite=unittest.TestSuite()

# 第二步将测试用例加载到测试套件中

## 第1种通过模块去加载
# from 单元测试.day13 import testcase
# loader=unittest.TestLoader()  # 创建一个加载对象
# suite.addTest(loader.loadTestsFromModule(testcase))
## 第2种通过测试用例类去加载
# from 单元测试.day13 import testcase
# loader=unittest.TestLoader()
# suite.addTest(loader.loadTestsFromTestCase(testcase.LoginTestCase))

## 第3种 添加单条测试用例
# from 单元测试.day13.testcase import LoginTestCase
# case=LoginTestCase("test_login_pass") #通过测试用例类创建测用例试对象时，需要传入测试用例的方法名(字符串)

## 第4种 通过路径加载
loader=unittest.TestLoader()
suite.addTest(loader.discover(r"E:\python-automation\单元测试\day13"))
# 第三步创建一个测试运行程序启动器
runner=unittest.TextTestRunner()

# 第四步使用启动器去执行测试套件
runner.run(suite)
