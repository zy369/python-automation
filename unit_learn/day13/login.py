# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 13:24
# @Author  : daodao
# @FileName: login.py
# @Software: PyCharm
# @qq ：402583874

def login_check(username=None,password=None):
    '''
    登陆校验的函数
    :param username:账号
    :param password:密码
    :return: dict type
    '''
    if all([username,password]):
       if  username=='python' and password=='123' :
            return {"code":0,"msg":"登录成功"}
       else:
            return {"code": 1, "msg": "账号或密码不正确"}
    else:
        return {"code": 1, "msg": "所有的参数不能为空"}